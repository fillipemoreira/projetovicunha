//
//  EmendasCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 07/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class EmendasCell: UITableViewCell {
	
	@IBOutlet weak var lblEmenda: UILabel!
	@IBOutlet weak var lblContrato: UILabel!
	@IBOutlet weak var lblDataGeracao: UILabel!
	@IBOutlet weak var lblVendedor: UILabel!
	@IBOutlet weak var lblTipo: UILabel!
	@IBOutlet weak var lblIdAprovacaoEmenda: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
}

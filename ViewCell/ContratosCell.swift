//
//  ContratosCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 06/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class ContratosCell: UITableViewCell {
	
	// Contratos
	@IBOutlet weak var lblContrato: UILabel!
	@IBOutlet weak var lblDataGeracao: UILabel!
	@IBOutlet weak var lblVendedor: UILabel!
	@IBOutlet weak var lblTipoPreco: UILabel!
	@IBOutlet weak var lblIdAprovacao: UILabel!
	
	// Preços
	@IBOutlet weak var lblNomePreco: UILabel!
	@IBOutlet weak var lblIdPreco: UILabel!
	@IBOutlet weak var lblValorPreco: UILabel!
	
    //Entregas
	@IBOutlet weak var lbl01: UILabel!
	
	// Indice Referencia
    @IBOutlet weak var lblIndiceReferencia: UILabel!
	
	// Documentos
	@IBOutlet weak var lblDocumentoC: UILabel!
	@IBOutlet weak var lblNomeC: UILabel!
	@IBOutlet weak var lblTipoC: UILabel!
	@IBOutlet weak var lblDataC: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

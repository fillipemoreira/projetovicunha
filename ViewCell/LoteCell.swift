//
//  LoteCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 20/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class LoteCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lblNome: UILabel!
    
	
	override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

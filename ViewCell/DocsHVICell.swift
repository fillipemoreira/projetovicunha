//
//  DocsHVICell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 20/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DocsHVICell: UITableViewCell {

	
	@IBOutlet weak var lblDocumento: UILabel!
	@IBOutlet weak var lblNome: UILabel!
	@IBOutlet weak var lblTipo: UILabel!
	@IBOutlet weak var lblData: UILabel!
	
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

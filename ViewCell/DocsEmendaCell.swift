//
//  DocsEmendaCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 08/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DocsEmendaCell: UITableViewCell {
	
    // Alteração
    @IBOutlet weak var lblDocumentoE: UILabel!
	@IBOutlet weak var lblNomeE: UILabel!
	@IBOutlet weak var lblTipoE: UILabel!
	@IBOutlet weak var lblDataE: UILabel!
	
    // Fixação
    @IBOutlet weak var lblDocumentoF: UILabel!
    @IBOutlet weak var lblNomeDocF: UILabel!
    @IBOutlet weak var lblTipoDocF: UILabel!
    @IBOutlet weak var lblDataDocF: UILabel!
    
}

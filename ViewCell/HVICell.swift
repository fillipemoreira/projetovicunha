//
//  HVICell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 21/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class HVICell: UITableViewCell {

	// Contratos
   

    @IBOutlet weak var lblContrato: UILabel!
    @IBOutlet weak var lblQtdeEmbarque: UILabel!
    @IBOutlet weak var lblVendedor: UILabel!
    @IBOutlet weak var lblDataGeracao: UILabel!
    // Documentos
	@IBOutlet weak var lblDocumentoC: UILabel!
	@IBOutlet weak var lblNomeC: UILabel!
	@IBOutlet weak var lblTipoC: UILabel!
	@IBOutlet weak var lblDataC: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
}

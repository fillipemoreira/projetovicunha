//
//  PagamentosCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 19/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class PagamentosCell: UITableViewCell {
	
	@IBOutlet weak var lblContrato: UILabel!
	@IBOutlet weak var lblDataGeracao: UILabel!
	@IBOutlet weak var lblVendedor: UILabel!
	@IBOutlet weak var lblTipoPagamento: UILabel!
    @IBOutlet weak var lblValorPagar: UILabel!
    override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
}

//
//  SettingsLauncher.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 19/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class SettingsLauncher: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
	let container: UIView = UIView()
	
	let blackView = UIView()
	let collectionView: UICollectionView = {
		let layout = UICollectionViewFlowLayout()
		let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
		cv.backgroundColor = UIColor.white
		return cv
	}()
	
	let cellId = "cellId"
	let cellHeight: CGFloat = CGFloat(50)
	let settingsLocal: [Settings] = {
		let settingsSetting = Settings(name: .Settings, imageName: "iconSettings.png")
		let settingHelp = Settings(name: .Help, imageName: "iconHelp.png")
		let cancelSetting = Settings(name: .Cancel, imageName: "iconCancel.png")
		return [ settingsSetting, settingHelp, cancelSetting ]
	}()
	
	var contratosViewController: ContratosViewController?
	
	func showSettings() {
		if let window = UIApplication.shared.keyWindow {
			
			self.blackView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
			
			//let tapGesture = UITapGestureRecognizer(target: self, action: #selector()) // handleDismiss(_:)
			//tapGesture.numberOfTapsRequired = 1
			//tapGesture.numberOfTouchesRequired = 1
			
			self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
			
			window.addSubview(self.blackView)
			window.addSubview(self.collectionView)
			
			let height = CGFloat(settingsLocal.count) * cellHeight
			let y = window.frame.height - height
			collectionView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
			
			self.blackView.frame = window.frame
			self.blackView.alpha = 0
			
			UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
				self.blackView.alpha = 1
				self.collectionView.frame = CGRect(x: 0,y: y, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
			}, completion: nil)
		}
	}
	
	@objc private func handleDismiss(settingLocal: Settings) {
		
		UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
			self.blackView.alpha = 0
			
			if let window = UIApplication.shared.keyWindow {
				self.collectionView.frame = CGRect(x: 0,y: window.frame.height, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
			}
	}) { (completed: Bool) in
		if settingLocal.name != .Cancel {
			self.contratosViewController?.showControllerForSetting(settingLocal: settingLocal)
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return settingsLocal.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingCell
		
		let setting = settingsLocal[indexPath.item]
		cell.setting = setting
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: collectionView.frame.width, height: cellHeight)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let settingLocal = self.settingsLocal[indexPath.item]
		handleDismiss(settingLocal: settingLocal)
	}
	
	override init() {
		super.init()
		
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.register(SettingCell.self, forCellWithReuseIdentifier: cellId)
	}
}

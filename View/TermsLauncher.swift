//
//  TermsLauncher.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 21/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit
import PureLayout

class TermsLauncher: NSObject {

	var aceite: String!
	var profile = [Profile]()
	let blackView = UIView()
	
	lazy var loginViewController: LoginViewController = {
		let loginLocal = LoginViewController()
		loginLocal.termsLauncher = self
		return loginLocal
	}()
	
	let whiteView: UIView = {
		let layout = UICollectionViewFlowLayout()
		let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
		cv.backgroundColor = UIColor.white
		
		return cv
	}()
	
	lazy var lblTerms: UILabel = {
		let label = UILabel()
		label.restorationIdentifier = "lblTerms"
		label.font = UIFont.systemFont(ofSize: 15.0)
		label.numberOfLines = 2
		label.text = MyVariables.traducaoTermos
		label.lineBreakMode = NSLineBreakMode.byWordWrapping
		
		return label
	}()
	
	lazy var swtTerms: UISwitch = {
		let swt = UISwitch()
		swt.addTarget(self, action: #selector(handleAccept), for: UIControl.Event.valueChanged)
		swt.setOn(false, animated: true)
		
		return swt
	}()
	
	@objc private func handleAccept(sender: UISwitch){
		if(sender.isOn) {
			_ = DataBaseHelper.shareInstance.updateProfile(object: ["aceite":"S"], index: 0)
			self.profile = DataBaseHelper.shareInstance.getProfileData()
			DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
				self.whiteView.removeFromSuperview()
				self.blackView.removeFromSuperview()
				self.loginViewController.performSegue(withIdentifier: "homeSegue", sender: nil)
				})
		} 
	}
	
	func showTermo() {
		if let window = UIApplication.shared.keyWindow {
			
			self.blackView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
			
			window.addSubview(self.blackView)
			window.addSubview(self.whiteView)
			
			self.whiteView.addSubview(lblTerms)
			lblTerms.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
			lblTerms.autoPinEdge(.top, to: .top, of: self.whiteView, withOffset: 20)
			
			self.whiteView.addSubview(swtTerms)
			swtTerms.autoPinEdge(toSuperviewEdge: .top, withInset: 360)
			swtTerms.autoPinEdge(toSuperviewEdge: .left, withInset: 250)
			
			let varHeight = CGFloat(window.frame.height * 0.7)
			let y = window.frame.height - varHeight
			whiteView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: varHeight)
			
			self.blackView.frame = window.frame
			self.blackView.alpha = 0
			
			UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
				self.blackView.alpha = 1
				self.whiteView.frame = CGRect(x: 0,y: y, width: self.whiteView.frame.width, height: self.whiteView.frame.height)
			}, completion: nil)
		}
	}
	
	override init() {
		super.init()
	}
}

//
//  Settings.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 20/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class Settings: NSObject {

	let name: SettingName
	let imageName: String
	
	init(name: SettingName, imageName: String) {
		self.name = name
		self.imageName = imageName
	}
}

//
//  SettingCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 20/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class SettingCell: BaseCell {

	override var isHighlighted: Bool {
		didSet{
			backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white
			nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.black
			// iconImageView.tintColor = isHighlighted ? UIColor.white : UIColor.darkGray // APENAS PARA IMAGES P&B
		}
	}
	
	var setting: Settings? {
		didSet {
			nameLabel.text = setting?.name.rawValue
			
			if let imageName = setting?.imageName {
				iconImageView.image = UIImage(named: imageName) //?.withRenderingMode(.alwaysTemplate)
				// iconImageView.tintColor = UIColor.darkGray
			}
		}
	}
	
	let nameLabel: UILabel = {
		let label = UILabel()
		label.text = "Settings"
		label.font = UIFont.systemFont(ofSize: 13)
		label.textAlignment = .center
		return label
	}()
	
	let iconImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.image = UIImage(named: "iconSettings.png")
		imageView.contentMode = .scaleAspectFill
		return imageView
	}()
	
	override func setupViews() {
		super.setupViews()
		
		addSubview(nameLabel)
		addSubview(iconImageView)
		
		nameLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 45)
		nameLabel.autoPinEdge(.top, to: .top, of: self, withOffset: 10)
		
		iconImageView.autoPinEdge(toSuperviewEdge: .left, withInset: 8)
		iconImageView.autoPinEdge(.top, to: .top, of: self, withOffset: 5)
	}
}

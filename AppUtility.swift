//
//  AppUtility.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 11/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//
import Foundation
import UIKit

struct AppUtility {
	
	static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
		
		if let delegate = UIApplication.shared.delegate as? AppDelegate {
			delegate.orientationLock = orientation
		}
	}
	
	static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
		
		self.lockOrientation(orientation)
		
		UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
		UINavigationController.attemptRotationToDeviceOrientation()
	}
	
    static func showAlertController(sender: UIViewController, _ title: String, _ message: String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
		sender.present(alertController, animated: true, completion: nil)
	}
	
	static func topController(_ parent: UIViewController? = nil) -> UIViewController {
		if let vc = parent {
			if let tab = vc as? UITabBarController, let selected = tab.selectedViewController {
				return topController(selected)
			} else if let nav = vc as? UINavigationController, let top = nav.topViewController {
				return topController(top)
			} else if let presented = vc.presentedViewController {
				return topController(presented)
			} else {
				return vc
			}
		} else {
			return topController(UIApplication.shared.keyWindow!.rootViewController!)
		}
	}
}

//
//  Enums.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 27/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

enum SettingName: String {
	case Cancel = "Cancel"
	case Settings = "Settings"
	case Help = "Help"
}

//
//  Window.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 14/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
	
	func visibleViewController() -> UIViewController? {
		if let rootViewController: UIViewController = self.rootViewController {
			return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
		}
		return nil
	}
	
	class func getVisibleViewControllerFrom(vc: UIViewController) -> UIViewController {
		
		switch(vc){
		case is UINavigationController:
			let navigationController = vc as! UINavigationController
			return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
			
		case is UITabBarController:
			let tabBarController = vc as! UITabBarController
			return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
			
		default:
			if let presentedViewController = vc.presentedViewController {
				//print(presentedViewController)
				if let presentedViewController2 = presentedViewController.presentedViewController {
					return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController2)
				}
				else{
					return vc;
				}
			}
			else{
				return vc;
			}
		}
		
	}
	
}

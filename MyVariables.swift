//
//  MyVariable.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 28/01/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

struct MyVariables {
	
	// Traduções
	static let traducaoMenu = NSLocalizedString("traducaoMenu", comment: "traducaoMenu")
	static let traducaoTituloLogOut = NSLocalizedString("traducaoTituloLogOut", comment: "traducaoTituloLogOut")
	static let traducaoMensagemLogOut = NSLocalizedString("traducaoMensagemLogOut", comment: "traducaoMensagemLogOut")
	static let traducaoBtLogOut = NSLocalizedString("traducaoBtLogOut", comment: "traducaoBtLogOut")
	static let traducaoBtClose = NSLocalizedString("traducaoBtClose", comment: "traducaoBtClose")
	static let traducaoBiometria = NSLocalizedString("traducaoBiometria", comment: "traducaoBiometria")
	static let traducaoS = NSLocalizedString("traducaoS", comment: "traducaoS")
	static let traducaoAceite = NSLocalizedString("traducaoAceite", comment: "traducaoAceite")
	static let traducaoNao = NSLocalizedString("traducaoNao", comment: "traducaoNao")
	static let traducaoSim = NSLocalizedString("traducaoSim", comment: "traducaoSim")
	static let traducaoAceiteTermos = NSLocalizedString("traducaoAceiteTermos", comment: "traducaoAceiteTermos")
	static let traducaoWaitingTouch = NSLocalizedString("traducaoWaitingTouch", comment: "traducaoWaitingTouch")
	static let traducaoAlerta = NSLocalizedString("traducaoAlerta", comment: "traducaoAlerta")
	static let traducaoDadosInvalidos = NSLocalizedString("traducaoDadosInvalidos", comment: "traducaoDadosInvalidos")
	static let traducaoTermos = NSLocalizedString("traducaoTermos", comment: "traducaoTermos")
	static let traducaoReprovacao = NSLocalizedString("traducaoReprovacao", comment: "traducaoReprovacao")
	static let traducaoAprovacao = NSLocalizedString("traducaoAprovacao", comment: "traducaoAprovacao")
	static let traducaoReprovacaoContrato = NSLocalizedString("traducaoReprovacaoContrato", comment: "traducaoReprovacaoContrato")
	static let traducaoReprovadoSucesso = NSLocalizedString("traducaoReprovadoSucesso", comment: "traducaoReprovadoSucesso")
	static let traducaoAprovacaoContrato = NSLocalizedString("traducaoAprovacaoContrato", comment: "traducaoAprovacaoContrato")
	static let traducaoAprovadoSucesso = NSLocalizedString("traducaoAprovadoSucesso", comment: "traducaoAprovadoSucesso")
	static let traducaoInformeMotivo = NSLocalizedString("traducaoInformeMotivo", comment: "traducaoInformeMotivo")
	static let traducaoMotivo = NSLocalizedString("traducaoMotivo", comment: "traducaoMotivo")
	static let traducaoConfirmaAprovacao = NSLocalizedString("traducaoConfirmaAprovacao", comment: "traducaoConfirmaAprovacao")
	static let traducaoCancelar = NSLocalizedString("traducaoCancelar", comment: "traducaoCancelar")
	static let traducaoDetalhes = NSLocalizedString("traducaoDetalhes", comment: "traducaoDetalhes")
	static let traducaoFalhaAutenticacao = NSLocalizedString("traducaoFalhaAutenticacao", comment: "traducaoFalhaAutenticacao")
	static let traducaoTouchInativo = NSLocalizedString("traducaoTouchInativo", comment: "traducaoTouchInativo")
	static let traducaoAprovacaoEmenda = NSLocalizedString("traducaoAprovacaoEmenda", comment: "traducaoAprovacaoEmenda")
	static let traducaoReprovacaoEmenda = NSLocalizedString("traducaoReprovacaoEmenda", comment: "traducaoReprovacaoEmenda")
	
	// Usuário
	let usuarioStored = UserDefaults.standard.string(forKey: "txtUsuario")
	let hashUser = UserDefaults.standard.string(forKey: "hashUser")
}

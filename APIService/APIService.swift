//
//  APIService.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 09/01/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIService: NSObject {
	
	public var iID = 0
	public var retorno = ""
	
	var hashUser: String {
		get {
			return UserDefaults.standard.string(forKey: "hashUser")!
		}
	}

	func executeGetQuantidadePendencias(completion: @escaping ((_ response: [Aprovacoes]) -> Void)) {
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetQuantidadePendencias"
		let headerLocal = ["Authorization": self.hashUser]
		var arrayOfAprovacoes = [] as [Aprovacoes]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno =  response.result.value
			
			switch response.result {
				case .success:
					arrayOfAprovacoes = Aprovacoes.aprovacoesJSON(paramEntrada: jsonRetorno!)
					completion(arrayOfAprovacoes)
				case .failure(let error):
					let topController = AppUtility.topController()
					AppUtility.showAlertController(sender: topController, "EmendasPendentes - Error serializing json",  error.localizedDescription)
			}
		}
	}
}

//
//  APIHVI.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 22/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIHVI: APIService {
	
	override init() {
		super.init()
	}
	
	func executeGetHVIList(completion: @escaping ((_ response: [HVIPendentes]) -> Void)) {
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetHvisPendentes"
		let headerLocal = ["Authorization": self.hashUser]
		var arrayOfHviPendentes = [] as [HVIPendentes]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				if (jsonRetorno != "") {
					arrayOfHviPendentes = HVIPendentes.hviJSON(paramEntrada: jsonRetorno!)
					
					if arrayOfHviPendentes.count == 0 {
						let window: UIWindow? = nil
						if let topController = window?.visibleViewController() {
							AppUtility.showAlertController(sender: topController, "Erro", "Erro de conexão com Internet")
						}
					} else {
						completion(arrayOfHviPendentes)
					}
				}
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDetalhesHVI(completion: @escaping ((_ response: [DetalhesHVI]) -> Void)) {
		
		let parametersLocal = ["idContratoEmbarque": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetDetalhesHVI/"
		var detalhesHVI = [] as [DetalhesHVI]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				detalhesHVI = DetalhesHVI.detalhesHVIJSON(paramEntrada: jsonRetorno!)
				completion(detalhesHVI)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDocumentosByContrato(completion: @escaping ((_ response: [DocumentosHVI]) -> Void)) {
		
		let parametersLocal = ["idContratoEmbarque": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetDocumentosByHVI/"
		var arrayofDocumentos = [] as [DocumentosHVI]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.queryString, headers: headerLocal).responseString() {
			response in
			
			let statusCode = (response.response?.statusCode)!
			
			if (statusCode == 200) {
				let jsonRetorno = response.result.value
				
				switch response.result {
				case .success:
					arrayofDocumentos = DocumentosHVI.documentosHVIJSON(paramEntrada: jsonRetorno!)
					completion(arrayofDocumentos)
				case .failure(let error):
					let topController = AppUtility.topController()
					AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
				}
			} else {
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", "\(statusCode)")
			}
		}
	}
	/*
	func base64ToFileDocContrato(completion: @escaping ((_ response: [ImgDocsContrato]) -> Void)) {
		
		let parametersLocal = ["idContratoDocumento": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetImageDocumento"
		var arrayOfImgDocsContrato = [] as [ImgDocsContrato]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno =  response.result.value
			
			switch response.result {
			case .success:
				arrayOfImgDocsContrato = ImgDocsContrato.imgDocsContratoJSON(paramEntrada: jsonRetorno!)
				completion(arrayOfImgDocsContrato)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executePutAprovarReprovarContrato(completion: @escaping ((_ response: String) -> Void)) {
		//         https://algodaoapi.vicunha.com/api/aprovacoes/aprovareprovacontrato/?idAprovacaoContrato=4393&aprova=false&dsMotivo="teste"
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/aprovareprovacontrato/"
		let headerLocal = ["Authorization": self.hashUser, "Content-Type": "application/json"]
		
		self.iID = 3217
		// "dsMotivo": String("\"\"")
		
		let parametersLocal = ["idAprovacao": self.iID, "aprova": String(false), "dsMotivo": "Teste Reprova"] as [String : Any]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .put, parameters: parametersLocal, encoding: URLEncoding.httpBody, headers: headerLocal).responseString() {
			response in
			
			let statusCode = (response.response?.statusCode)!
			let index = parametersLocal.index(forKey: "aprova")
			let valorParam = parametersLocal[index!].value as? String
			
			if (statusCode == 200) {
				if (valorParam == "true") {
					self.retorno = MyVariables.traducaoAprovadoSucesso
				} else {
					self.retorno = MyVariables.traducaoReprovadoSucesso
				}
			} else {
				self.retorno = "Erro: \(statusCode)"
			}
			completion(self.retorno)
		}
	}
	*/
}

//
//  APIPagamento.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 22/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIPagamento: APIService {
	
	func executeGetPagamentosPendentes(completion: @escaping ((_ response: [PagamentosPendentes]) -> Void)) {
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetPagamentosPendentes"
		var arrayOfPagamentosPendentes = [] as [PagamentosPendentes]
		let headerLocal = ["Authorization": self.hashUser]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				if (jsonRetorno != "") {
					arrayOfPagamentosPendentes = PagamentosPendentes.pagamentosPendentesJSON(paramEntrada: jsonRetorno!)
				}
				completion(arrayOfPagamentosPendentes)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDetalhesHVI(completion: @escaping ((_ response: [DetalhesEmenda]) -> Void)) {
		
		let parametersLocal = ["idAprovacaoEmenda": self.iID]
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetDetalhesEmenda/"
		var detalhesEmenda = [] as [DetalhesEmenda]
		let headerLocal = ["Authorization": self.hashUser]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				detalhesEmenda = DetalhesEmenda.detalhesEmendaJSON(paramEntrada: jsonRetorno!)
				completion(detalhesEmenda)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDocumentosByEmenda(completion: @escaping ((_ response: [DocumentosEmenda]) -> Void)) {
		
		let parametersLocal = ["idEmenda": self.iID]
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetDocumentosByEmenda"
		var arrayofDocumentosEmenda = [] as [DocumentosEmenda]
		let headerLocal = ["Authorization": self.hashUser]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.queryString, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				if jsonRetorno! != "" {
					arrayofDocumentosEmenda = DocumentosEmenda.documentosEmendaJSON(paramEntrada: jsonRetorno!)
					if arrayofDocumentosEmenda.count > 0 {
						completion(arrayofDocumentosEmenda)
					}
				}
				
				
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func json(from object:Any) -> Data? {
		guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
			return nil
		}
		return data
	}
	
	func executePutAprovarReprovarEmenda(parametersLocal: [String : Any],completion: @escaping ((_ response: String) -> Void)) {
		
		//let url = "https://algodaoapi.vicunha.com/api/aprovacoes/aprovareprovaemenda"
		//let headerLocal = ["Authorization": self.hashUser]
		//let parametersLocal = ["idContratoDocumento": self.iID]
		
		
		
		//let encoder = JSONEncoder()
		//let jsonData = try! encoder.encode(product)
		let jsonArray = json(from:parametersLocal)
		//let url = "INSERT_URL"
		let api = "https://algodaoapi.vicunha.com/api/aprovacoes/aprovareprovaemenda"
		
		if let postURL = URL(string: api) {
			var request = URLRequest(url: postURL)
			
			
			//var request = URLRequest(url:url)
			request.httpMethod = HTTPMethod.put.rawValue
			request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
			request.setValue(self.hashUser, forHTTPHeaderField: "Authorization")
			//let encoder = JSONEncoder()
			request.httpBody = jsonArray
			
			Alamofire.request(request).responseJSON { response in
				let statusCode = (response.response?.statusCode)!
				let index = parametersLocal.index(forKey: "aprova")
				let valorParam = parametersLocal[index!].value as? String
				
				if (statusCode == 200) {
					if (valorParam == "true") {
						self.retorno = MyVariables.traducaoAprovadoSucesso
					} else {
						self.retorno = MyVariables.traducaoReprovadoSucesso
					}
				} else {
					self.retorno = "Erro: \(statusCode)"
				}
				completion(self.retorno)
			}
		}
		
		
		/*
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .put, parameters: parametersLocal, encoding: URLEncoding.queryString, headers: headerLocal).responseString() {
		response in
		
		let statusCode = (response.response?.statusCode)!
		let index = parametersLocal.index(forKey: "aprova")
		let valorParam = parametersLocal[index!].value as? String
		
		if (statusCode == 200) {
		if (valorParam == "true") {
		self.retorno = MyVariables.traducaoAprovadoSucesso
		} else {
		self.retorno = MyVariables.traducaoReprovadoSucesso
		}
		} else {
		self.retorno = "Erro: \(statusCode)"
		}
		completion(self.retorno)
		}
		*/
		
		
	}
	
	func base64ToFileDocEmenda(completion: @escaping ((_ reponse: [ImgDocsEmenda]) -> Void)) {
		
		let parametersLocal = ["idContratoDocumento": self.iID]
		
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetImageDocumento"
		var arrayOfImgDocsEmenda = [] as [ImgDocsEmenda]
		let headerLocal = ["Authorization": self.hashUser]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				arrayOfImgDocsEmenda = ImgDocsEmenda.imgDocsEmendaJSON(paramEntrada: jsonRetorno!)
				completion(arrayOfImgDocsEmenda)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
}

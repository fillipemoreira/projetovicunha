//
//  APIContrato.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 22/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIContrato: APIService {
	
	override init() {
		super.init()
	}
	
	func executeGetContratosPendentes(completion: @escaping ((_ response: [ContratosPendentes]) -> Void)) {
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetContratosPendentes"
		let headerLocal = ["Authorization": self.hashUser]
		var arrayOfContratosPendentes = [] as [ContratosPendentes]

		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				if (jsonRetorno != "") {
					arrayOfContratosPendentes = ContratosPendentes.contratosPendentesJSON(paramEntrada: jsonRetorno!)
					
					if arrayOfContratosPendentes.count == 0 {
						let window: UIWindow? = nil
						if let topController = window?.visibleViewController() {
							AppUtility.showAlertController(sender: topController, "Erro", "Erro de conexão com Internet")
						}
					} else {
						completion(arrayOfContratosPendentes)
					}
				}
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDetalhesContrato(completion: @escaping ((_ response: [DetalhesContrato]) -> Void)) {
		
		let parametersLocal = ["idAprovacaoContrato": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/GetDetalhesContrato/"
		var detalhesContrato = [] as [DetalhesContrato]
	
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
			
			let jsonRetorno = response.result.value
			
			switch response.result {
			case .success:
				detalhesContrato = DetalhesContrato.detalhesContratoJSON(paramEntrada: jsonRetorno!)
				completion(detalhesContrato)
			case .failure(let error):
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func executeGetDocumentosByContrato(completion: @escaping ((_ response: [DocumentosContrato]) -> Void)) {
		
		let parametersLocal = ["idContrato": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetDocumentosByContrato"
		var arrayofDocumentosContrato = [] as [DocumentosContrato]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.queryString, headers: headerLocal).responseString() {
			response in

			let statusCode = (response.response?.statusCode)!
			
			if (statusCode == 200) {
				let jsonRetorno = response.result.value
			
				switch response.result {
				case .success:
					arrayofDocumentosContrato = DocumentosContrato.documentosContratoJSON(paramEntrada: jsonRetorno!)
					completion(arrayofDocumentosContrato)
				case .failure(let error):
					let topController = AppUtility.topController()
					AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
				}
			} else {
				let topController = AppUtility.topController()
				AppUtility.showAlertController(sender: topController, "Erro", "\(statusCode)")
			}
		}
	}
	
	func base64ToFileDocContrato(completion: @escaping ((_ response: [ImgDocsContrato]) -> Void)) {
	
		let parametersLocal = ["idContratoDocumento": self.iID]
		let headerLocal = ["Authorization": self.hashUser]
		
		let url = "https://algodaoapi.vicunha.com/api/Documento/GetImageDocumento"
		var arrayOfImgDocsContrato = [] as [ImgDocsContrato]
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .get, parameters: parametersLocal, encoding: URLEncoding.default, headers: headerLocal).responseString() {
			response in
		
			let jsonRetorno =  response.result.value
		
			switch response.result {
				case .success:
					arrayOfImgDocsContrato = ImgDocsContrato.imgDocsContratoJSON(paramEntrada: jsonRetorno!)
					completion(arrayOfImgDocsContrato)
				case .failure(let error):
					let topController = AppUtility.topController()
					AppUtility.showAlertController(sender: topController, "Erro", error.localizedDescription)
			}
		}
	}
	
	func json(from object:Any) -> Data? {
		guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
			return nil
		}
		return data
	}
	
	func executePutAprovarReprovarContrato( parametersLocal: [String : Any],completion: @escaping ((_ response: String) -> Void)) {
		//         https://algodaoapi.vicunha.com/api/aprovacoes/aprovareprovacontrato/?idAprovacaoContrato=4393&aprova=false&dsMotivo="teste"
		//let url = "https://algodaoapi.vicunha.com/api/Aprovacoes/aprovareprovacontrato/"
		//let headerLocal = ["Authorization": self.hashUser, "Content-Type": "application/json"]
		
		//self.iID = 3217
		// "dsMotivo": String("\"\"")
		
		//let parametersLocal = ["idAprovacao": self.iID, "aprova": String(true), "dsMotivo": "Teste Reprova"] as [String : Any]
		
		
		
		
		
		
		//let encoder = JSONEncoder()
		//let jsonData = try! encoder.encode(product)
		let jsonArray = json(from:parametersLocal)
		//let url = "INSERT_URL"
		let api = "https://algodaoapi.vicunha.com/api/Aprovacoes/aprovareprovacontrato/"
		
		if let postURL = URL(string: api) {
			var request = URLRequest(url: postURL)
			
	
			//var request = URLRequest(url:url)
			request.httpMethod = HTTPMethod.put.rawValue
			request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
			request.setValue(self.hashUser, forHTTPHeaderField: "Authorization")
			//let encoder = JSONEncoder()
			request.httpBody = jsonArray
			
			Alamofire.request(request).responseJSON { response in
				let statusCode = (response.response?.statusCode)!
				let index = parametersLocal.index(forKey: "aprova")
				let valorParam = parametersLocal[index!].value as? String
				
				if (statusCode == 200) {
					if (valorParam == "true") {
						self.retorno = MyVariables.traducaoAprovadoSucesso
					} else {
						self.retorno = MyVariables.traducaoReprovadoSucesso
					}
				} else {
					self.retorno = "Erro: \(statusCode)"
				}
				completion(self.retorno)
			}
		}
		
		//var request = URLRequest(url:url2)

		/*
		
		let manager = Alamofire.SessionManager.default
		manager.request(url, method: .put, parameters: parametersLocal, encoding: URLEncoding.httpBody, headers: headerLocal).responseString() {
			response in
			
			let statusCode = (response.response?.statusCode)!
			let index = parametersLocal.index(forKey: "aprova")
			let valorParam = parametersLocal[index!].value as? String
			
			if (statusCode == 200) {
				if (valorParam == "true") {
					self.retorno = MyVariables.traducaoAprovadoSucesso
				} else {
					self.retorno = MyVariables.traducaoReprovadoSucesso
				}
			} else {
				self.retorno = "Erro: \(statusCode)"
			}
			completion(self.retorno)
		}
*/
		
	}
}

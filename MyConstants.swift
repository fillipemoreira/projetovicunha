//
//  MyConstants.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 02/04/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation
import UIKit

struct MyConstants {
	
	// Emendas
	static let fixacaoUpper = String(String("fixação").folding(options: .diacriticInsensitive, locale: .current)).uppercased()
	static let alteracaoUpper = String(String("alteração").folding(options: .diacriticInsensitive, locale: .current)).uppercased()
	
	// Layout
	static let borderTableView = CGFloat(1.5)
	static let colorTableView = UIColor.black.cgColor
}

//
//  ViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 05/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var container: ContainerViewController!
	
    @IBOutlet weak var segmentControl: UISegmentedControl!
	
    @IBOutlet weak var lblContContrato: UILabel!
    @IBOutlet weak var lblContEmenda: UILabel!
    @IBOutlet weak var lblContPagamento: UILabel!
    @IBOutlet weak var lblContHVI: UILabel!
    
    
    override func viewDidLoad() {
        
		super.viewDidLoad()
        
        self.formataContador(sender: lblContContrato)
		self.formataContador(sender: lblContEmenda)
        self.formataContador(sender: lblContPagamento)
        self.formataContador(sender: lblContHVI)
		
		self.segmentControl.setTitle("Contrato", forSegmentAt: 0)
		self.segmentControl.setTitle("Emenda", forSegmentAt: 1)
		self.segmentControl.setTitle("HVI", forSegmentAt: 2)
		self.segmentControl.setTitle("$", forSegmentAt: 3)
	}
	
    private func formataContador(sender: UILabel) {
        sender.layer.cornerRadius = 5
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.masksToBounds = true
		sender.layer.borderWidth = 1
    }
    
	override func viewDidAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.carregaDadosAprovacoesPendentes()
	}
	
	private func carregaDadosAprovacoesPendentes() {
		let apiService = APIService()
		apiService.executeGetQuantidadePendencias() {
			arrayOfAprovacoesLocal in
			
			if (arrayOfAprovacoesLocal.count > 0) {
                self.lblContContrato.text = String(arrayOfAprovacoesLocal[0].vAprovacoesContrato)
				self.lblContEmenda.text = String(arrayOfAprovacoesLocal[0].vAprovacoesEmenda)
                self.lblContPagamento.text = String(arrayOfAprovacoesLocal[0].vAprovacoesPagamento)
                self.lblContHVI.text = String(arrayOfAprovacoesLocal[0].vAprovacoesEmbarqueLote)
			}
		}
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            container!.segueIdentifierReceivedFromParent("first")
            self.title = "Contrato"
        } else if sender.selectedSegmentIndex == 1  {
            container!.segueIdentifierReceivedFromParent("second")
            self.title = "Emenda"
		} else if sender.selectedSegmentIndex == 2  {
			container!.segueIdentifierReceivedFromParent("third")
			self.title = "HVI"
		} else if sender.selectedSegmentIndex == 3  {
			container!.segueIdentifierReceivedFromParent("fourth")
			self.title = "$"
		}
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "container" {
			container = segue.destination as? ContainerViewController
 
            container.animationDurationWithOptions = (0.5, .transitionCrossDissolve)
        }
    }
}

    extension ViewController: SecondViewDelegate{
        func sendToThirdViewController() {
            container.segueIdentifierReceivedFromParent("viewc")
        }
    }

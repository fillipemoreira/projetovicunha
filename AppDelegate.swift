//
//  AppDelegate.swift
//  PrototipoVicunha001
//
//  Created by Ronney Nigro on 01/11/18.
//  Copyright © 2018 Ronney Nigro. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
	
	//lock orientation
	//https://stackoverflow.com/questions/28938660/how-to-lock-orientation-of-one-view-controller-to-portrait-mode-only-in-swift
	
	var window: UIWindow?
	var orientationLock = UIInterfaceOrientationMask.all
	
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
		
		print("Token: \(token)")
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print(error)
	}
	
	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
		return self.orientationLock
	}
	
	func setupPushNotification(application: UIApplication) {

		let center = UNUserNotificationCenter.current()
		center.delegate = self
		center.requestAuthorization(options: [.badge, .alert, .sound], completionHandler: { (granted, error) in
			if granted {
				DispatchQueue.main.async(execute: {
					application.registerForRemoteNotifications()
				})
			} else {
				print("Push Notification permission Danied: \(error?.localizedDescription ?? "error")")
			}
		})
		//application.registerForRemoteNotifications()
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler([[.alert, .sound, .badge]])
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		if let notification = response.notification.request.content.userInfo as? [String:AnyObject] {
			let message = parseRemoteNotification(notification: notification)
			print(message as Any)
		}
		
		completionHandler()
	}
	
	func parseRemoteNotification(notification: [String:AnyObject]) -> String? {
		if let aps = notification["aps"] as? [String:AnyObject] {
			let alert = aps["alert"] as? String
			return alert
		} else {
			return nil
		}
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		// self.setupPushNotification(application: application)
		
		self.splashScreen()
		
		UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
		
		UIApplication.shared.registerForRemoteNotifications()
		
		UNUserNotificationCenter.current().delegate = self
		UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
			if granted {
				DispatchQueue.main.async(execute: {
					application.registerForRemoteNotifications()
				})
			}
			UINavigationBar.appearance().tintColor = UIColor.black // Cor da Fonte
			UINavigationBar.appearance().shadowImage = UIImage()
			UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
			
		// print("Document Directory: ", FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last ?? "Not found SQLite")
		}
		
		return true
	}
	
	private func splashScreen() {
		let launchScreenVC = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
		let rootVC = launchScreenVC.instantiateViewController(withIdentifier: "splashController")
		
		self.window?.rootViewController = rootVC
		self.window?.makeKeyAndVisible()
		Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
	}
	
	@objc func dismissSplashController() {
		let mainVC = UIStoryboard.init(name: "Main", bundle: nil)
		let rootVC = mainVC.instantiateViewController(withIdentifier: "loginController")
		self.window?.rootViewController = rootVC
		self.window?.makeKeyAndVisible()
	}
	
	private func setarStatusBar() {
		let statusBarBackGroundView = UIView()
		self.window?.addSubview(statusBarBackGroundView)
		self.window?.addConstraintsWithFormat(format: "H:|[v0]|", views: statusBarBackGroundView)
		self.window?.addConstraintsWithFormat(format: "V:[v0(5)]", views: statusBarBackGroundView)
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		// Saves changes in the application's managed object context before the application terminates.
		self.saveDefaulContext()
	}
	
	// MARK: - Core Data stack
	lazy var persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: "Projeto_Vicunha")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()
	
	// MARK: - Core Data Saving support
	func saveDefaulContext () {
		let context = persistentContainer.viewContext
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}
	
}

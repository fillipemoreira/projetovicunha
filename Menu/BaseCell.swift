//
//  BaseCell.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 03/01/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {

	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupViews()
	}
	
	func setupViews() {
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has no been implemented")
	}
}

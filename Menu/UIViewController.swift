//
//  UIView.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 17/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

extension UIView {
	
	func addConstraintsWithFormat(format: String, views: UIView...) {
		
		var viewDictionary = [String: UIView]()
		
		for (index, view) in views.enumerated() {
			let key = "v\(index)"
			view.translatesAutoresizingMaskIntoConstraints = false
			viewDictionary[key] = view
		}
		
		addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDictionary))
	}
}

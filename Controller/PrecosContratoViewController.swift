//
//  PrecosContratoViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 18/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class PrecosContratoViewController: UIViewController {
	
	var detalhesContrato = [DetalhesContrato]()
	var activityIndicatorView: UIActivityIndicatorView!
	var iId = 0
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.iId = indexPath.row
		
		tableView.deselectRow(at: indexPath, animated: false)
		
		goEntregas()
	}
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let entregasContratoViewController: EntregasContratoViewController = segue.destination as! EntregasContratoViewController
		entregasContratoViewController.detalhesContrato = self.detalhesContrato
		entregasContratoViewController.idPreco = self.iId
    }

    @objc func goEntregas() {
        self.performSegue(withIdentifier: "segueEntregas", sender: nil)
    }
}

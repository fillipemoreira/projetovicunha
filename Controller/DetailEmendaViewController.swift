//
//  DetailEmendaViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 07/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DetailEmendaViewController:  UIViewController, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate {
	
	private var detalhesEmenda = [DetalhesEmenda]()
	var idAprovacaoEmenda = 0
	var motivo = ""
	private var documentosEmendaF = [DocumentosEmenda]()
	var idDocumento = 0
	var idEmenda = 0
	var sExtensao = ""
	
    @IBOutlet weak var lblEmenda: UILabel!
    @IBOutlet weak var lblContrato: UILabel!
    @IBOutlet weak var lblVendedor: UILabel!
    @IBOutlet weak var lblEmissao: UILabel!
    @IBOutlet weak var lblTipo: UILabel!
    @IBOutlet weak var lblCorretora: UILabel!
    @IBOutlet weak var lblDescricao: UILabel!
    @IBOutlet weak var lblObservacao: UILabel!
	
    @IBOutlet weak var lblTEmenda: UILabel!
    @IBOutlet weak var lblTContrato: UILabel!
    @IBOutlet weak var lblTVendedor: UILabel!
    @IBOutlet weak var lblTEmissao: UILabel!
    @IBOutlet weak var lblTTipo: UILabel!
    @IBOutlet weak var lblTCorretora: UILabel!
    @IBOutlet weak var lblTDescricao: UILabel!
    @IBOutlet weak var lblTObservacao: UILabel!
	
    @IBOutlet weak var btAprovarE: UIButton!
    @IBOutlet weak var btReprovarE: UIButton!
    
    @IBOutlet weak var lblDocumentosEmenda: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
    @IBOutlet weak var tableViewDocsEmenda: UITableView!
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.documentosEmendaF.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "DocsEmendaCell", for: indexPath) as? DocsEmendaCell
			else { return UITableViewCell() }
		
		cell.lblDocumentoE.text = "\(self.documentosEmendaF[indexPath.row].Documento)"
		cell.lblNomeE.text = self.documentosEmendaF[indexPath.row].Nome
		cell.lblTipoE.text = self.documentosEmendaF[indexPath.row].Tipo
		cell.lblDataE.text = self.documentosEmendaF[indexPath.row].Data
		
		self.activityIndicator.stopAnimating()
		return cell
	}
	
    private func exibeTextoDetalhesEntrega() {
		
		self.lblEmenda.isHidden = false
		self.lblContrato.isHidden = false
		self.lblVendedor.isHidden = false
		self.lblEmissao.isHidden = false
		self.lblTipo.isHidden = false
		self.lblCorretora.isHidden = false
		self.lblDescricao.isHidden = false
		self.lblObservacao.isHidden = false
		
		self.lblTEmenda.isHidden = false

		self.lblTContrato.isHidden = false
		self.lblTVendedor.isHidden = false
		self.lblTEmissao.isHidden = false
		self.lblTTipo.isHidden = false
		self.lblTCorretora.isHidden = false
		self.lblTDescricao.isHidden = false
		self.lblTObservacao.isHidden = false
		
		self.btAprovarE.isHidden = false
		self.btReprovarE.isHidden = false
        
        lblDocumentosEmenda.isHidden = false
        
        tableViewDocsEmenda.isHidden = false
	}
	
	private func carregaDadosDetalhesEmenda() {
		let apiEmenda = APIEmenda()
		apiEmenda.iID = self.idAprovacaoEmenda
		apiEmenda.executeGetDetalhesEmenda() {arrayOfEmendasPendentesLocal in
			self.detalhesEmenda = arrayOfEmendasPendentesLocal
			if (self.detalhesEmenda.count > 0) {
				DispatchQueue.main.async {
					self.lblEmenda.text = self.detalhesEmenda[0].Emenda
					self.lblContrato.text = self.detalhesEmenda[0].Contrato
					self.lblVendedor.text = self.detalhesEmenda[0].Vendedor
					self.lblEmissao.text = self.detalhesEmenda[0].Emissao
					self.lblTipo.text = self.detalhesEmenda[0].Tipo
					self.lblCorretora.text = self.detalhesEmenda[0].Corretora
					self.lblDescricao.text = self.detalhesEmenda[0].Descricao
					self.lblObservacao.text = self.detalhesEmenda[0].ObsEmenda
				}
			}
			self.activityIndicator.stopAnimating()
			self.exibeTextoDetalhesEntrega()
		}
	}
	
	private func carregaDadosDocumentosEmenda() {
		let apiEmenda = APIEmenda()
		apiEmenda.iID = self.idEmenda
		apiEmenda.executeGetDocumentosByEmenda() {arrayOfDocumentosEmendaLocal in
			self.documentosEmendaF = arrayOfDocumentosEmendaLocal
			DispatchQueue.main.async {
				self.tableViewDocsEmenda.reloadData()
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let img = UIImage()
		self.navigationController?.navigationBar.shadowImage = img
		self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
		
		self.carregaDadosDetalhesEmenda()
		self.carregaDadosDocumentosEmenda()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.title = "Detalhes Emenda"
	}
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		self.tableViewDocsEmenda.delegate = self
		self.tableViewDocsEmenda.dataSource = self
		
		self.tableViewDocsEmenda.allowsSelection = true
		self.tableViewDocsEmenda.delaysContentTouches = false
		
		self.title = "Documentos Emenda"
		
		activityIndicator.startAnimating()
		
		self.defineBotoes()
		self.defineGrades()
	}
	
	func defineGrades() {
		tableViewDocsEmenda.layer.borderWidth = MyConstants.borderTableView
		tableViewDocsEmenda.layer.borderColor = MyConstants.colorTableView
	}
	
	func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
		sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.borderWidth = 2
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	func defineBotoes() {
        self.aplicaValoresBotao(sender: btAprovarE)
        self.aplicaValoresBotao(sender: btReprovarE)
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.idDocumento = self.documentosEmendaF[indexPath.row].Documento
		
		let sArquivo = self.documentosEmendaF[indexPath.row].Nome
		let sSeparador = sArquivo.index(of: ".")!
		self.sExtensao = String(sArquivo[sSeparador...]).uppercased()
		
		tableView.deselectRow(at: indexPath, animated: false)
		
		self.goVisualizaDocEmenda()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let visualizaDocEViewController: VisualizaDocEViewController = segue.destination as! VisualizaDocEViewController
		visualizaDocEViewController.iDocumento = self.idDocumento
		visualizaDocEViewController.sExtensao = self.sExtensao
	}
	
    @IBAction func btAprovarE(_ sender: UIButton) {
        let alertController = UIAlertController(title: MyVariables.traducaoAprovacaoEmenda, message: MyVariables.traducaoConfirmaAprovacao, preferredStyle: .alert)
        
        let sim = UIAlertAction(title: MyVariables.traducaoSim, style: .default, handler: self.AprovarEmenda)
        alertController.addAction(sim)
        
        let nao = UIAlertAction(title: MyVariables.traducaoNao, style: .cancel)
        alertController.addAction(nao)
        
        self.present(alertController, animated: true, completion: nil)
    }
	
    @objc func goVisualizaDocEmenda() {
		self.performSegue(withIdentifier: "segueVisiualizaDocE", sender: nil)
	}
	
    @IBAction func btReprovarEmenda(_ sender: UIButton) {
        let alertController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: MyVariables.traducaoInformeMotivo, preferredStyle: .alert)
		
		let sim = UIAlertAction(title: MyVariables.traducaoReprovacao, style: .default, handler: {action in
			self.motivo = alertController.textFields![0].text!
			
			if (self.motivo != "") {
				let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: MyVariables.traducaoReprovadoSucesso  + ": " + self.motivo, preferredStyle: .alert)
				let ok = UIAlertAction(title: "OK", style: .default, handler: self.HistoryBackEmenda)
				confirmacaoController.addAction(ok)
				self.ReprovarEmenda()
				self.present(confirmacaoController, animated: true, completion: nil)
			} else {
				AppUtility.showAlertController(sender: self, "Erro", "campoMotivoObrigatorio")
			}
		})
		alertController.addAction(sim)
		
		alertController.addTextField { (textField) in
			textField.placeholder = MyVariables.traducaoMotivo
		}
		
		let nao = UIAlertAction(title: MyVariables.traducaoNao, style: .cancel)
		alertController.addAction(nao)
        
        self.present(alertController, animated: true, completion: nil)
    }
	
	func AprovarEmenda(alert: UIAlertAction) {
		
		let apiEmenda = APIEmenda()
		let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(true), "dsMotivo": String("\"\"")] as [String : Any]
		
		self.activityIndicator.startAnimating()
		apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
			retornoLocal in

			let confirmacaoController = UIAlertController(title: MyVariables.traducaoAprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
			
			let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		self.activityIndicator.stopAnimating()
	}
	
	func ReprovarEmenda() {
		
		let apiEmenda = APIEmenda()
		let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(false), "dsMotivo": self.motivo] as [String : Any]
		
		self.activityIndicator.startAnimating()
		apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
			retornoLocal in
			
			let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
			
			let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		self.activityIndicator.stopAnimating()
	}
		

	func HistoryBackEmenda(alert: UIAlertAction) {
		self.navigationController?.popViewController(animated: true)
	}
	
	//	DocsEmendaViewController.idDocumento = self.iIdDocumento
	//	DocsEmendaViewController.idEmenda = self.iIdEmenda
}

//
//  EntregasContratoViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 21/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class EntregasContratoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	var detalhesContrato = [DetalhesContrato]()
	var activityIndicatorView: UIActivityIndicatorView!
	var idPreco = -1
	
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.detalhesContrato[0].Preco[idPreco].Entregas.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // https://gist.github.com/mchirico/50cdb07d20b1b0f73d7c
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
			else { return UITableViewCell() }
		
		cell.lbl01.text = detalhesContrato[0].Preco[idPreco].Entregas[indexPath.row]
		
		self.activityIndicatorView.stopAnimating()
		return cell
	}
	
	private func carregaDadosEntregasContrato() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}
	
	override func loadView() {
		super.loadView()
		activityIndicatorView = UIActivityIndicatorView(style: .gray)
		
		tableView.backgroundView = activityIndicatorView
	}
	
	override func viewDidAppear(_ animated: Bool) {
		self.carregaDadosEntregasContrato()
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		
		self.tableView.allowsSelection = true
		self.tableView.delaysContentTouches = false
		
		activityIndicatorView.startAnimating()
		
		self.title = "Entregas Contrato"
	}
}

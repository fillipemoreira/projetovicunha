//
//  VisualizaDocCViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 27/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit
import WebKit

class VisualizaDocCViewController: UIViewController {
	
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var arrayOfImgDocsContrato = [ImgDocsContrato]()
	var iDocumento = 0
    var sExtensao = ""
	var sNomeArq = ""
	
	lazy var previewItem = NSURL()
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewDidAppear(_ animated: Bool) {

	}
	
	private func carregaArquivo() {
		let apiContrato = APIContrato()
		apiContrato.iID = self.iDocumento
		apiContrato.base64ToFileDocContrato() { arrayOfImgDocsContratoLocal in
			self.arrayOfImgDocsContrato = arrayOfImgDocsContratoLocal
			self.exibeDocumento()
            
        self.activityIndicator.stopAnimating()
		}
	}
	
    private func exibeDocumento() {
		var sMimeType = ""
		let decodeLocal = self.arrayOfImgDocsContrato[0].imgDocumento
		let data = Data(base64Encoded: decodeLocal)
		let webView = WKWebView(frame: CGRect(x: 0, y: 75, width: self.view.frame.size.width, height: self.view.frame.size.height))
		self.view.addSubview(webView)
		//https://gist.github.com/ngs/918b07f448977789cf69
		if (self.sExtensao == ".XLSX") {
			
			let path = Bundle.main.path(forResource: self.sNomeArq, ofType: self.sExtensao)
			do {
				let content = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
				print(content)
			} catch {
				print("nil")
			}
			
			//guard let docURL = Bundle(for: type(of: self)).url(forResource: self.sNomeArq, withExtension: self.sExtensao) else {
			//	fatalError("File not found")
			//}
			
			//let docURL = Bundle.main.url(forResource: self.sNomeArq, withExtension: self.sExtensao)!
			
		//	let docContents = try! Data(contentsOf: path)
		//	let urlStr = "data:application/msexcel;base64," + docContents.base64EncodedString() // msword
		//	let url = URL(string: urlStr)!
		//	let request = URLRequest(url: path)
			
			//webView.load(request)
			
			// sMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		} else if (self.sExtensao == ".XLS") {
			sMimeType = "application/vnd.ms-excel"
		} else if (self.sExtensao == ".PDF") {
			sMimeType = "application/pdf"
			webView.load(data!, mimeType: sMimeType, characterEncodingName: "UTF-8", baseURL: URL(string: "https://www.swift.com")!)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.carregaArquivo()
	}
}

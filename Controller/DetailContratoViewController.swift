//
//  DetailContratoViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 21/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class DetailContratoViewController: UIViewController, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
	private var detalhesContrato = [DetalhesContrato]()
    private var documentosContrato = [DocumentosContrato]()
	
	var iIdAprovacaoContrato = 0
	var idContrato = 0
	var iDocumento = 0
	var motivo = ""
	var sExtensao = ""
	var sNomeArq = ""
	
	// Detalhes Contrato
    @IBOutlet weak var lblTContratoC: UILabel!
    @IBOutlet weak var lblTEmissaoC: UILabel!
    @IBOutlet weak var lblTVendedorC: UILabel!
    @IBOutlet weak var lblTCorretoraC: UILabel!
    @IBOutlet weak var lblTFornecedorC: UILabel!
    @IBOutlet weak var lblTRegiaoC: UILabel!
    @IBOutlet weak var lblTEntregaC: UILabel!
    @IBOutlet weak var lblTSafraC: UILabel!
    @IBOutlet weak var lblTQuantidadeC: UILabel!
    @IBOutlet weak var lblTQualidadeC: UILabel!
    @IBOutlet weak var lblTPagamentoC: UILabel!
    @IBOutlet weak var lblTObservacaoC: UILabel!
    
    @IBOutlet weak var lblContratoC: UILabel!
    @IBOutlet weak var lblEmissaoC: UILabel!
    @IBOutlet weak var lblVendedorC: UILabel!
    @IBOutlet weak var lblCorretoraC: UILabel!
    @IBOutlet weak var lblFornecedorC: UILabel!
    @IBOutlet weak var lblRegiaoC: UILabel!
    @IBOutlet weak var lblEntregaC: UILabel!
    @IBOutlet weak var lblSafraC: UILabel!
    @IBOutlet weak var lblQuantidadeC: UILabel!
    @IBOutlet weak var lblQualidadeC: UILabel!
    @IBOutlet weak var lblQualidadeTXTC: UILabel!
    @IBOutlet weak var lblPagamentoC: UILabel!
    @IBOutlet weak var lblObservacaoC: UILabel!

	// Precos
	@IBOutlet weak var lblTPrecos: UILabel!
	@IBOutlet weak var tableViewPrecos: UITableView!
    
	// Indices Referência
	@IBOutlet weak var lblTIndiceRef: UILabel!
	@IBOutlet weak var tableViewIndicesRef: UITableView!
    
    // Documentos
	@IBOutlet weak var lblTDocumentos: UILabel!
	@IBOutlet weak var tableViewDocumentos: UITableView!
	
	// Botões
	@IBOutlet weak var btReprovar: UIButton!
	@IBOutlet weak var btAprovar: UIButton!
	
	// Geral
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	private func mensagemTableView(paramTitulo: String) -> UILabel {
		
		let label = UILabel()
		label.text = paramTitulo
		label.font = UIFont.systemFont(ofSize: 13)
		label.textColor = UIColor.red
		label.textAlignment = .center
		
		return label
	}
	
    private func exibeTextoDetalhesContrato() {
		self.lblContratoC.isHidden = false
		self.lblEmissaoC.isHidden = false
		self.lblVendedorC.isHidden = false
		self.lblCorretoraC.isHidden = false
		self.lblFornecedorC.isHidden = false
		self.lblRegiaoC.isHidden = false
		self.lblEntregaC.isHidden = false
		self.lblSafraC.isHidden = false
		self.lblQuantidadeC.isHidden = false
		self.lblQualidadeTXTC.isHidden = false
		self.lblQualidadeC.isHidden = false
		self.lblPagamentoC.isHidden = false
		self.lblObservacaoC.isHidden = false
        
        self.lblTContratoC.isHidden = false
        self.lblTEmissaoC.isHidden = false
        self.lblTVendedorC.isHidden = false
        self.lblTCorretoraC.isHidden = false
        self.lblTFornecedorC.isHidden = false
        self.lblTRegiaoC.isHidden = false
        self.lblTEntregaC.isHidden = false
        self.lblTSafraC.isHidden = false
        self.lblTQuantidadeC.isHidden = false
        self.lblTQualidadeC.isHidden = false
        self.lblTPagamentoC.isHidden = false
        self.lblTObservacaoC.isHidden = false
		
        self.btAprovar.isHidden = false
        self.btReprovar.isHidden = false
		
		exibeFilhosContrato()
	}
	
	private func exibeFilhosContrato() {
		self.lblTPrecos.isHidden = false
		self.tableViewPrecos.isHidden = false
		
		self.lblTIndiceRef.isHidden = false
		self.tableViewIndicesRef.isHidden = false
		
		self.lblTDocumentos.isHidden = false
		self.tableViewDocumentos.isHidden = false
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
		
		// Detalhes Contrato
		if (self.detalhesContrato.count > 0) {
			
        	if (tableView == self.tableViewPrecos) {
				count = self.detalhesContrato[0].Preco.count
				if (count == 0) {
					self.tableViewPrecos.backgroundView = self.mensagemTableView(paramTitulo: "Sem Informações de 'Preços'")
				}
			}

        
        	if (tableView == self.tableViewIndicesRef) {
				if (self.detalhesContrato[0].IndicesReferencia != nil) {
					count =  self.detalhesContrato[0].IndicesReferencia.descricoes.count
				} else {
					self.tableViewIndicesRef.backgroundView = self.mensagemTableView(paramTitulo: "Sem Informações de 'Índice Referência'")
				}
        	}
		}
		
		// Documentos Contrato
		if (self.documentosContrato.count > 0) {
			if (tableView == self.tableViewDocumentos) {
				count = self.documentosContrato.count
				if (count == 0) {
					self.tableViewDocumentos.backgroundView = self.mensagemTableView(paramTitulo: "Sem Informações de 'Documentos Contrato'")
				}
			}
		}
		
        return count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cellMaster : UITableViewCell?
		
		// Preços
		if tableView == self.tableViewPrecos {
			
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
				else { return UITableViewCell() }
            
			if let idPreco = self.detalhesContrato[0].Preco[indexPath.row].idIndicePreco {
				cell.lblIdPreco.text = "\(idPreco)"
			} else {
				cell.lblIdPreco.text = "_"
			}
		
			cell.lblNomePreco.text = "\(self.detalhesContrato[0].Preco[indexPath.row].dsNome)"
		
			if let dPreco = self.detalhesContrato[0].Preco[indexPath.row].vlPreco {
				cell.lblValorPreco.text = "\(dPreco)"
			} else {
				cell.lblValorPreco.text = "_"
			}
			
			cellMaster = cell
		}
		
		// Índices Referência
		if tableView == self.tableViewIndicesRef {
			
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
				else { return UITableViewCell() }
			
			cell.lblIndiceReferencia.text = self.detalhesContrato[0].IndicesReferencia.descricoes[indexPath.row]
			
			cellMaster = cell
		}
		
		// Documentos Contrato
		if tableView == self.tableViewDocumentos {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
				else { return UITableViewCell() }
		
			cell.lblDocumentoC.text = "\(self.documentosContrato[indexPath.row].Documento)"
			cell.lblNomeC.text = self.documentosContrato[indexPath.row].Nome
			cell.lblTipoC.text = self.documentosContrato[indexPath.row].Tipo
			cell.lblDataC.text = self.documentosContrato[indexPath.row].Data
			
			cellMaster = cell
		}
	
		return cellMaster!
	}
	
	private func carregaDadosPrecosContrato() {
		DispatchQueue.main.async {
        //	self.tableViewPrecos.reloadData()
		}
	}
	
    private func carregaDadosDetailContrato() {
		let apiContrato = APIContrato()
		apiContrato.iID = self.iIdAprovacaoContrato
		apiContrato.executeGetDetalhesContrato() {arrayOfDetalhesContratoLocal in
			self.detalhesContrato = arrayOfDetalhesContratoLocal
			if (self.detalhesContrato.count > 0) {
				DispatchQueue.main.async {
					self.lblContratoC.text = self.detalhesContrato[0].Contrato
					self.lblEmissaoC.text = self.detalhesContrato[0].Emissao
					self.lblVendedorC.text = self.detalhesContrato[0].Vendedor
					self.lblCorretoraC.text = self.detalhesContrato[0].Corretora
					self.lblFornecedorC.text = self.detalhesContrato[0].Fornecedor
					self.lblRegiaoC.text = self.detalhesContrato[0].Regiao
					self.lblEntregaC.text = self.detalhesContrato[0].Entrega
					self.lblSafraC.text = self.detalhesContrato[0].Safra
					self.lblQuantidadeC.text = self.detalhesContrato[0].Quantidade
					self.lblQualidadeC.text = self.detalhesContrato[0].Qualidade.qualidade
					self.lblQualidadeTXTC.text = self.detalhesContrato[0].Qualidade.obsQualidade
                	self.lblPagamentoC.text = self.detalhesContrato[0].Pagamento
					
					if let sObservacao = self.detalhesContrato[0].ObsContrato {
						self.lblObservacaoC.text = sObservacao
					} else {
						self.lblObservacaoC.text = "_"
					}
					
					if (self.detalhesContrato[0].IndicesReferencia != nil) {
						self.lblTIndiceRef.text = "Índices Referência: \(self.detalhesContrato[0].IndicesReferencia.data)"
					}
					
					self.tableViewPrecos.reloadData()
					self.tableViewIndicesRef.reloadData()
					self.tableViewDocumentos.reloadData()
				}
			}
			self.activityIndicator.stopAnimating()
            self.exibeTextoDetalhesContrato()
		}
	}
	
	private func carregaDadosDocumentosContrato() {
		let apiContrato = APIContrato()
		apiContrato.iID = self.idContrato
		apiContrato.executeGetDocumentosByContrato() {documentosContratoLocal in
			self.documentosContrato = documentosContratoLocal
			DispatchQueue.main.async {
				self.tableViewDocumentos.reloadData()
				self.activityIndicator.stopAnimating()
			}
		}
		
		if (self.documentosContrato.count == 0) {
			self.activityIndicator.stopAnimating()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
		
		self.carregaDadosDetailContrato()
		self.carregaDadosDocumentosContrato()
		
		self.activityIndicator.stopAnimating()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		activityIndicator.startAnimating()
		self.title = "Detalhes Contrato"
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}

	override func viewDidLoad() {
		
		super.viewDidLoad()
		
        tableViewPrecos.dataSource = self
        tableViewPrecos.delegate = self
        tableViewPrecos.register(UITableViewCell.self, forCellReuseIdentifier: "precosCell")
        
        tableViewIndicesRef.dataSource = self
        tableViewIndicesRef.delegate = self
        tableViewIndicesRef.register(UITableViewCell.self, forCellReuseIdentifier: "indicesRefCell")
        
        tableViewDocumentos.dataSource = self
        tableViewDocumentos.delegate = self
        tableViewDocumentos.register(UITableViewCell.self, forCellReuseIdentifier: "documentosCell")
        
		self.defineBotoes()
        self.defineGrades()
	}
    
	func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
		sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.borderWidth = 2
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func defineGrades() {
        tableViewPrecos.layer.borderWidth = MyConstants.borderTableView
        tableViewPrecos.layer.borderColor = MyConstants.colorTableView
        
        tableViewIndicesRef.layer.borderWidth = MyConstants.borderTableView
        tableViewIndicesRef.layer.borderColor = MyConstants.colorTableView
        
        tableViewDocumentos.layer.borderWidth = MyConstants.borderTableView
        tableViewDocumentos.layer.borderColor = MyConstants.colorTableView
    }
    
	func defineBotoes() {
		self.aplicaValoresBotao(sender: btReprovar)
		self.aplicaValoresBotao(sender: btAprovar)
	}
	
	@IBAction func btReprovar(_ sender: UIButton) {
		let alertController = UIAlertController(title: MyVariables.traducaoReprovacaoContrato, message: MyVariables.traducaoInformeMotivo, preferredStyle: .alert)
		
		let sim = UIAlertAction(title: MyVariables.traducaoReprovacao, style: .default, handler: {action in
			self.motivo = alertController.textFields![0].text!
			
			if (self.motivo != "") {
				let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoContrato, message: MyVariables.traducaoReprovadoSucesso  + ": " + self.motivo, preferredStyle: .alert)
				let ok = UIAlertAction(title: "OK", style: .default, handler: self.HistoryBack)
				confirmacaoController.addAction(ok)
				self.Reprovar()
				self.present(confirmacaoController, animated: true, completion: nil)
			} else {
				AppUtility.showAlertController(sender: self, "Erro", "campoMotivoObrigatorio")
			}
		})
		alertController.addAction(sim)
		
		alertController.addTextField { (textField) in
			textField.placeholder = MyVariables.traducaoMotivo
		}
		
		let nao = UIAlertAction(title: MyVariables.traducaoCancelar, style: .cancel)
		alertController.addAction(nao)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	@IBAction func btAprovar(_ sender: UIButton) {
		let alertController = UIAlertController(title: MyVariables.traducaoAprovacaoContrato, message: MyVariables.traducaoConfirmaAprovacao, preferredStyle: .alert)
		
		let nao = UIAlertAction(title: MyVariables.traducaoNao, style: .cancel)
		alertController.addAction(nao)
		
		let sim = UIAlertAction(title: MyVariables.traducaoSim, style: .default, handler: self.Aprovar)
		alertController.addAction(sim)
		
		self.present(alertController, animated: true, completion: nil)
    }
	
	func Aprovar(alert: UIAlertAction) {
		
		let apiContrato = APIContrato()

		let parametersLocal = ["idAprovacao": self.iIdAprovacaoContrato, "aprova": String(true), "dsMotivo": ""] as [String : Any]
		self.activityIndicator.startAnimating()
		apiContrato.executePutAprovarReprovarContrato(parametersLocal:parametersLocal) {
			retornoLocal in

			let confirmacaoController = UIAlertController(title: MyVariables.traducaoAprovacaoContrato, message: retornoLocal, preferredStyle: .alert)
			
			let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBack)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		self.activityIndicator.stopAnimating()
	}
	
	func Reprovar() {
		let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoContrato, message: MyVariables.traducaoReprovadoSucesso, preferredStyle: .alert)
		let apiContrato = APIContrato()
		activityIndicator.startAnimating()
		let parametersLocal = ["idAprovacao": self.iIdAprovacaoContrato, "aprova": String(false), "dsMotivo": self.motivo] as [String : Any]
		apiContrato.executePutAprovarReprovarContrato(parametersLocal:parametersLocal) {
			retornoLocal in
			
			let ok = UIAlertAction(title: retornoLocal, style: .default, handler: self.HistoryBack)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		activityIndicator.stopAnimating()
	}
    
	func HistoryBack(alert: UIAlertAction) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
		
		if tableView == self.tableViewDocumentos {
			self.iDocumento = self.documentosContrato[indexPath.row].Documento
		
			let sArquivo = self.documentosContrato[indexPath.row].Nome
			let sSeparador = sArquivo.index(of: ".")!
			self.sExtensao = String(sArquivo[sSeparador...]).uppercased()
			self.sNomeArq = obterNomeArq(paramEntrada: sArquivo)
		
			tableView.deselectRow(at: indexPath, animated: false)
		
			self.goVisualizaDocContrato()
		}
	}
	
	private func obterNomeArq(paramEntrada: String) -> String {
		let greeting = paramEntrada
		let index = greeting.index(of: ".") ?? greeting.endIndex
		return String(greeting[..<index])
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if (segue.identifier == "segueVisiualizaDocC") {
			let visualizaDocCViewController: VisualizaDocCViewController = segue.destination as! VisualizaDocCViewController
			visualizaDocCViewController.iDocumento = self.iDocumento
			visualizaDocCViewController.sExtensao = self.sExtensao
			visualizaDocCViewController.sNomeArq = self.sNomeArq
		} else {
			AppUtility.showAlertController(sender: self, "Erro", "Segue: \(String(describing: segue.identifier))")
		}
	}
	
	@objc func goVisualizaDocContrato() {
		self.performSegue(withIdentifier: "segueVisiualizaDocC", sender: nil)
	}
	
	@objc func goEntregas() {
		self.performSegue(withIdentifier: "segueEntregas", sender: nil)
	}
}

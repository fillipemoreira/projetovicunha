//
//  MenuViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 30/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

	@IBOutlet weak var btAprovacoes: UIButton!
	@IBOutlet weak var btContratos: UIButton!
	@IBOutlet weak var btPrecos: UIButton!
	@IBOutlet weak var btPerfil: UIButton!
    @IBOutlet weak var btMessage: UIButton!
    
    @IBOutlet weak var lblPush: UILabel!
    @IBOutlet weak var imgBanner: UIImageView!
	
	
    @IBAction func btContratos(_ sender: Any) {
        //print(UIDevice.current.identifierForVendor?.description!)
		// https://www.techtudo.com.br/dicas-e-tutoriais/noticia/2012/09/o-que-e-udid.html
    }
	
	func getIFAddresses() -> [String] {
		var addresses = [String]()
		
		// Get list of all interfaces on the local machine:
		var ifaddr : UnsafeMutablePointer<ifaddrs>?
		guard getifaddrs(&ifaddr) == 0 else { return [] }
		guard let firstAddr = ifaddr else { return [] }
		
		// For each interface ...
		for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
			let flags = Int32(ptr.pointee.ifa_flags)
			let addr = ptr.pointee.ifa_addr.pointee
			
			// Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
			if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
				if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
					
					// Convert interface address to a human readable string:
					var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
					if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
									nil, socklen_t(0), NI_NUMERICHOST) == 0) {
						let address = String(cString: hostname)
						addresses.append(address)
					}
				}
			}
		}
		
		freeifaddrs(ifaddr)
		return addresses
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		//let pathImage = "https://catho.akamaized.net/empresas/destaque-quality-software.gif"
		//	"http://www.vicunhaservicos.com.br/wa_images/logo-vicunha-servicos.png"
		
		//do {
		//	let url = URL(string: pathImage)
		//	let data = try Data(contentsOf: url!)
		//	self.imgBanner.image = UIImage(data: data)
		//}
		//catch{
		//	print(error)
		//}
		
		self.defineBotoes()
        self.formataContador(sender: lblPush)
        
		navigationController?.hidesBarsOnSwipe = false
    }
	
    private func carregaDadosAprovacoesPendentes() {
        let apiService = APIService()
        apiService.executeGetQuantidadePendencias() {
            arrayOfAprovacoesLocal in
            
            if (arrayOfAprovacoesLocal.count > 0) {
                let total =
					arrayOfAprovacoesLocal[0].vAprovacoesContrato +
					arrayOfAprovacoesLocal[0].vAprovacoesEmenda +
					arrayOfAprovacoesLocal[0].vAprovacoesPagamento +
					arrayOfAprovacoesLocal[0].vAprovacoesEmbarqueLote

				self.btAprovacoes.setTitle("Aprovações:  [\(String(total))]", for: .normal)
            }
        }
    }

	private func defineBotoes() {
		self.aplicaValoresBotao(sender: btAprovacoes)
		self.aplicaValoresBotao(sender: btContratos)
		self.aplicaValoresBotao(sender: btMessage)
		self.aplicaValoresBotao(sender: btPrecos)
		self.aplicaValoresBotao(sender: btPerfil)
	}
	
	private func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
		sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.borderWidth = 2
	}
	
    private func formataContador(sender: UILabel) {
        sender.layer.cornerRadius = 5
        sender.layer.borderColor = UIColor.white.cgColor
        sender.layer.masksToBounds = true
        sender.layer.borderWidth = 1
    }
    
    @IBAction func btPerfil(_ sender: Any) {
		
		//let profileViewController: ProfileViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
		
		
		//let persistenceManager: PersistenceManager = PersistenceManager()
		//let profileViewController = ProfileViewController(persistenceManager: PersistenceManager)
		//profileViewController.
        
		//self.present(profileViewController, //animated: true, completion: nil)

    }
	
}

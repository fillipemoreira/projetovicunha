//
//  HVIViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 26/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class HVIViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	private var arrayOfHviPendentes = [HVIPendentes]()
	var IdContratoEmbarque = 0
	var idEmenda = 0
	var sTipo = ""
	
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityScreen: UIActivityIndicatorView!
    
	
	private func carregaDadosEmendasPendentes() {
		
		DispatchQueue.main.async {
			self.activityScreen.startAnimating()
		}
		
		let apiHvi = APIHVI()
		apiHvi.executeGetHVIList() {	 arrayOfEmendasPendentesLocal in
			self.arrayOfHviPendentes = arrayOfEmendasPendentesLocal
			DispatchQueue.main.async {
				self.tableView.reloadData()
				self.activityScreen.stopAnimating()
			}
		}
		
		if (self.arrayOfHviPendentes.count == 0) {
			DispatchQueue.main.async {
				self.tableView.reloadData()
				self.activityScreen.stopAnimating()
			}
			//self.activityScreen.stopAnimating()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let img = UIImage()
		self.navigationController?.navigationBar.shadowImage = img
		self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
		
		self.carregaDadosEmendasPendentes()
	}
	
	override func loadView() {
		super.loadView()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		//self.activityIndicatorView.startAnimating()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let titLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
		titLabel.text = "navigationItem.title"
		titLabel.textColor = UIColor.darkGray
		titLabel.font = UIFont.systemFont(ofSize: 15)
		//	navigationItem.titleView = titLabel
		
		self.tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		self.tableView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		
		self.setupNavBarButton()
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		
		self.tableView.allowsSelection = true
		self.tableView.delaysContentTouches = false
		
		//self.title = "Emendas Pendentes"
	}
	
	func setupNavBarButton() {
		let searchImage = UIImage(named: "iconSearch.png")?.withRenderingMode(.alwaysOriginal)
		_ = UIImage(named: "iconHamburger.png")?.withRenderingMode(.alwaysOriginal)
		
		let searchBarButtonItem = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: #selector(handleSearch))
		
		let moreButton = UIBarButtonItem(image: UIImage(named: "iconHamburger.png"), style: .plain, target: self, action: #selector(handleMore))
		
		navigationItem.rightBarButtonItems = [moreButton, searchBarButtonItem]
	}
	
	@objc private func handleSearch() {
		print(456)
	}
	
	lazy var settingsLauncher: SettingsLauncher = {
		let launcher = SettingsLauncher()
		return launcher
	}()
	
	@objc private func handleMore() {
		settingsLauncher.showSettings()
	}
	
	func showControllerForSetting(settingLocal: Settings) {
		let dummySettingsViewController = UIViewController()
		dummySettingsViewController.view.backgroundColor = UIColor.white
		dummySettingsViewController.navigationItem.title = settingLocal.name.rawValue
		navigationController?.navigationBar.tintColor = UIColor.black
		navigationController?.pushViewController(dummySettingsViewController, animated: true)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.arrayOfHviPendentes.count
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "HVICell", for: indexPath) as? HVICell
			else { return UITableViewCell() }
		
		//cell.lblEmenda.text = self.arrayOfHviPendentes[indexPath.row].Emenda
		cell.lblContrato.text = self.arrayOfHviPendentes[indexPath.row].Contrato
		cell.lblDataGeracao.text = self.arrayOfHviPendentes[indexPath.row].DataGeracao.description
		cell.lblVendedor.text = self.arrayOfHviPendentes[indexPath.row].Vendedor
		cell.lblQtdeEmbarque.text = self.arrayOfHviPendentes[indexPath.row].QtdeEmbarque.description
		//cell.lblTipo.text = self.arrayOfHviPendentes[indexPath.row].Tipo
		//cell.lblIdAprovacaoEmenda.text = String(self.arrayOfHviPendentes[indexPath.row].IdAprovacaoEmenda)
		
		return cell
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
		
		self.IdContratoEmbarque = self.arrayOfHviPendentes[indexPath.row].IdContratoEmbarque
		//self.idEmenda = self.arrayOfHviPendentes[indexPath.row].IdEmenda
		//self.sTipo = String(String(self.arrayOfHviPendentes[indexPath.row].Tipo).folding(options: .diacriticInsensitive, locale: .current)).uppercased()
		tableView.deselectRow(at: indexPath, animated: false)
		
		goDetails()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		
			let DetailHVIViewController: DetailHVIViewController = segue.destination as! DetailHVIViewController
			
			DetailHVIViewController.IdContratoEmbarque = self.IdContratoEmbarque
		
		//override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		//	if segue.identifier == "HistorySegue" {
		//		if let viewController = segue.destination as? HistoryController {
		//			if(barcodeInt != nil){
		//				viewController.detailItem = barcodeInt as AnyObject
		//			}
		//		}
		//	}
		//}
	}
	
	@objc func goDetails() {
		self.performSegue(withIdentifier: "segueDetalhesHVI", sender: nil)
	}
	
}

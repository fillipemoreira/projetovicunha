//
//  PagamentoViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 19/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class PagamentoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
	private var arrayOfPagamentosPendentes = [PagamentosPendentes]()
	var idAprovacaoEmenda = 0
	var idEmenda = 0
	var sTipo = ""
	
	//@IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var activityScreen: UIActivityIndicatorView!
    
	private func carregaDadosPagamentosPendentes() {
		
		DispatchQueue.main.async {
			self.activityScreen.startAnimating()
		}
		
		let apiPagamento = APIPagamento()
		apiPagamento.executeGetPagamentosPendentes() { arrayOfPagamentosPendentesLocal in
			self.arrayOfPagamentosPendentes = arrayOfPagamentosPendentesLocal
			DispatchQueue.main.async {
				self.tableView.reloadData()
				self.activityScreen.stopAnimating()
			}
		}
		
		if (self.arrayOfPagamentosPendentes.count == 0) {
			self.activityScreen.stopAnimating()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let img = UIImage()
		self.navigationController?.navigationBar.shadowImage = img
		self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
		
		self.carregaDadosPagamentosPendentes()
	}
	
	override func loadView() {
		super.loadView()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		//self.activityIndicatorView.startAnimating()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let titLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
		titLabel.text = "navigationItem.title"
		titLabel.textColor = UIColor.darkGray
		titLabel.font = UIFont.systemFont(ofSize: 15)
		//	navigationItem.titleView = titLabel
		
		self.tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		self.tableView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		
		self.setupNavBarButton()
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		
		self.tableView.allowsSelection = true
		self.tableView.delaysContentTouches = false
		
	}
	
	func setupNavBarButton() {
		let searchImage = UIImage(named: "iconSearch.png")?.withRenderingMode(.alwaysOriginal)
		_ = UIImage(named: "iconHamburger.png")?.withRenderingMode(.alwaysOriginal)
		
		let searchBarButtonItem = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: #selector(handleSearch))
		
		let moreButton = UIBarButtonItem(image: UIImage(named: "iconHamburger.png"), style: .plain, target: self, action: #selector(handleMore))
		
		navigationItem.rightBarButtonItems = [moreButton, searchBarButtonItem]
	}
	
	@objc private func handleSearch() {
		print(456)
	}
	
	lazy var settingsLauncher: SettingsLauncher = {
		let launcher = SettingsLauncher()
		return launcher
	}()
	
	@objc private func handleMore() {
		settingsLauncher.showSettings()
	}
	
	func showControllerForSetting(settingLocal: Settings) {
		let dummySettingsViewController = UIViewController()
		dummySettingsViewController.view.backgroundColor = UIColor.white
		dummySettingsViewController.navigationItem.title = settingLocal.name.rawValue
		navigationController?.navigationBar.tintColor = UIColor.black
		navigationController?.pushViewController(dummySettingsViewController, animated: true)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.arrayOfPagamentosPendentes.count
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "PagamentosCell", for: indexPath) as? PagamentosCell
			else { return UITableViewCell() }
		
		//cell.lblEmenda.text = self.arrayOfEmendasPendentes[indexPath.row].Emenda
		cell.lblContrato.text = self.arrayOfPagamentosPendentes[indexPath.row].Contrato
		cell.lblDataGeracao.text = self.arrayOfPagamentosPendentes[indexPath.row].DataGeracao.description
		cell.lblVendedor.text = self.arrayOfPagamentosPendentes[indexPath.row].Vendedor
		cell.lblTipoPagamento.text = self.arrayOfPagamentosPendentes[indexPath.row].TipoPagamento
        
        cell.lblValorPagar.text = self.arrayOfPagamentosPendentes[indexPath.row].ValorAPagar.description
		//cell.lblIdAprovacaoEmenda.text = String(self.arrayOfEmendasPendentes[indexPath.row].IdAprovacaoEmenda)
		
		return cell
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
		
		//self.idAprovacaoEmenda = self.arrayOfEmendasPendentes[indexPath.row].IdAprovacaoEmenda
		//self.idEmenda = self.arrayOfEmendasPendentes[indexPath.row].IdEmenda
		//self.sTipo = String(String(self.arrayOfEmendasPendentes[indexPath.row].Tipo).folding(options: .diacriticInsensitive, locale: .current)).uppercased()
		tableView.deselectRow(at: indexPath, animated: false)
		
		goDetails(paramTipo: sTipo)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if (segue.identifier == "segueDetalhesEmenda") {
			let DetailEmendaViewController: DetailEmendaViewController = segue.destination as! DetailEmendaViewController
			
			DetailEmendaViewController.idAprovacaoEmenda = self.idAprovacaoEmenda
			DetailEmendaViewController.idEmenda = self.idEmenda
		} else {
			let DetailEmendaFViewController: DetailEmendaFViewController = segue.destination as! DetailEmendaFViewController
			
			DetailEmendaFViewController.idAprovacaoEmenda = self.idAprovacaoEmenda
			DetailEmendaFViewController.idEmenda = self.idEmenda
		}
		//override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		//	if segue.identifier == "HistorySegue" {
		//		if let viewController = segue.destination as? HistoryController {
		//			if(barcodeInt != nil){
		//				viewController.detailItem = barcodeInt as AnyObject
		//			}
		//		}
		//	}
		//}
	}
	
	@objc func goDetails(paramTipo: String) {
		if (paramTipo == MyConstants.fixacaoUpper) {
			self.performSegue(withIdentifier: "segueDetalhesEmendaF", sender: nil)
		} else {
			self.performSegue(withIdentifier: "segueDetalhesEmenda", sender: nil)
		}
	}
	
	
}

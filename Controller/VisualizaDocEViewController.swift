//
//  VisualizaDocEViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 11/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit
import WebKit

class VisualizaDocEViewController: UIViewController {
	
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var arrayOfImgDocsEmenda = [ImgDocsEmenda]()
	var iDocumento = 0
	var sExtensao = ""
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewDidAppear(_ animated: Bool) {
	
	}
	
	private func carregaArquivo() {
		let apiEmenda = APIEmenda()
		apiEmenda.iID = self.iDocumento
		apiEmenda.base64ToFileDocEmenda() { arrayOfImgDocsEmendaLocal in
			self.arrayOfImgDocsEmenda = arrayOfImgDocsEmendaLocal
			self.exibeDocumentoEmenda()
		}
	}
	
	private func exibeDocumentoEmenda() {
		var sMimeType = ""
		let decodeLocal = self.arrayOfImgDocsEmenda[0].imgDocumento
		let data = Data(base64Encoded: decodeLocal)
		let webView = WKWebView(frame: CGRect(x: 0, y: 75, width: self.view.frame.size.width, height: self.view.frame.size.height))
		self.view.addSubview(webView)
		if (self.sExtensao == ".XLSX") {
			//let fromDataToString = String(data: download.fileContent!, encoding: .isoLatin1)
			let fromStringToData = Data(base64Encoded: data!, options: .ignoreUnknownCharacters)
			print(fromStringToData as Any)
			// sMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		} else if (self.sExtensao == ".XLS") {
			sMimeType = "text/xml" // application/vnd.ms-excel"
		} else if (self.sExtensao == ".PDF") {
			sMimeType = "application/pdf"
			webView.load(data!, mimeType: sMimeType, characterEncodingName: "", baseURL: URL(string: "https://www.swift.com")!)
		}
		
        self.activityIndicator.stopAnimating()
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.carregaArquivo()
    }
}

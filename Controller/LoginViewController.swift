//
//  LoginViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 08/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit
import LocalAuthentication
import PusherSwift
import UserNotifications

class LoginViewController: UIViewController, UITextFieldDelegate, PusherDelegate {
	
    @IBOutlet weak var btLogin: UIButton!
	@IBOutlet weak var btAuthenticate: UIButton!
	@IBOutlet weak var txtUsuario: UITextField!
	@IBOutlet weak var txtSenha: UITextField!
    @IBOutlet var viewLogin: UIView!
    
	var pusher: Pusher! = nil
	var profile = [Profile]()
	var aceiteLocal: String = ""
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		AppUtility.lockOrientation(.portrait)
	}
	
	override func viewDidLoad() {
        
        super.viewDidLoad()
		
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
		// gambi pra funcionar usuário logado no aparelho
		UserDefaults.standard.set("admin", forKey: "txtUsuario")
		UserDefaults.standard.set("123", forKey: "txtSenha")
		
		//lblUser.text = NSLocalizedString(JOy-xT-Zm3.title, comment: "")
		
		ListenPush()
        
        self.txtUsuario.delegate = self
        self.txtSenha.delegate = self
        
        self.defineBotoes()
	}
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
	
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.txtUsuario) {
            txtUsuario.resignFirstResponder()
        } else if (textField == self.txtSenha) {
            txtSenha.resignFirstResponder()
        }
  
        return false
    }
    
	func ShowTerms() {
		termsLauncher.showTermo()
	}
	
	lazy var termsLauncher: TermsLauncher = {
		let launcher = TermsLauncher()
		launcher.loginViewController = self
		return launcher
	}()
	
	func ListenPush() {

		let optionsPusher = PusherClientOptions(authMethod: .inline(secret: "420a22f3bd1c10d7b28f"), host: .cluster("us2"))
		pusher = Pusher(key: "167b9d1fcff20b8177e4", options: optionsPusher)
		let channel = pusher.subscribe("my-channel")
		
		pusher.delegate = self
		// PushManager.shared.sendLocalPush() = FuncionaLocal
		let _ = channel.bind(eventName: "my-event", callback: { (data: Any?) -> Void in
			if let data = data as? [String : AnyObject] {
				if let message = data["message"] as? String {
					PushManager.shared.sendLocalPush(message)
				}
			}
		})
		pusher.connect()
    }

	@IBAction func btAuthenticate(_ sender: Any) {
		let context = LAContext()
		var error: NSError?
		
		if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
			let reason = MyVariables.traducaoWaitingTouch
			context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
				{(succes, error) in
					if succes {			
						self.accessApplication()
					} else {
						AppUtility.showAlertController(sender: self, "Erro", MyVariables.traducaoFalhaAutenticacao)
					}
			})
		} else {
			AppUtility.showAlertController(sender: self, "Erro", MyVariables.traducaoTouchInativo)
		}
	}
	
	@IBAction func btLogin(_ sender: UIButton) {
		let txtUsuario = self.txtUsuario.text
		let txtSenha = self.txtSenha.text
		
		let usuarioStored = UserDefaults.standard.string(forKey: "txtUsuario")
		let senhaStored = UserDefaults.standard.string(forKey: "txtSenha")
		
		UserDefaults.standard.set("false", forKey: "isUserLoggedIn")
		
		if (usuarioStored == txtUsuario && senhaStored == txtSenha && !(txtUsuario == nil) && !(txtSenha == nil)) {
			accessApplication()
		} else {
			let alertController = UIAlertController(title: MyVariables.traducaoAlerta, message: MyVariables.traducaoDadosInvalidos, preferredStyle: .alert)
			
			let action = UIAlertAction(title: "OK", style: .cancel)
			alertController.addAction(action)
			self.present(alertController, animated: true, completion: nil)
		}
	}
	
    func defineBotoes() {
        self.aplicaValoresBotao(sender: btLogin)
        self.aplicaValoresBotao(sender: btAuthenticate)
    }
    
    func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
       	sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
       	sender.layer.borderWidth = 2
    }
    
	func accessApplication() {
		
		let parameters = ["Username": "015000715", "Password": "qawsedrf1234$#@!", "CdMacDispositivo": "testeMaycon"]
		let url = URL(string: "https://algodaoapi.vicunha.com/api/Login/")!
		let session = URLSession.shared
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		do {
			request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
		} catch let error {
			print(error.localizedDescription)
		}
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		//request.addValue("application/json", forHTTPHeaderField: "Accept")
		let task = session.dataTask(with: request as URLRequest, completionHandler: { dataR, response, error in
			
			guard error == nil else {
				print(error as Any)
				return
			}
			
			guard let data = dataR else {
				return
			}
			
			if let hashUserOld = String(bytes: data, encoding: .utf8) {
				let hashUser = hashUserOld.replacingOccurrences(of: "\"", with: "")

				UserDefaults.standard.set(hashUser, forKey: "hashUser")
			} else {
				print("not a valid UTF-8 sequence")
			}
			/*
			do {
				//create json object from data
				if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
					print(json)
					// handle json...
				}
			} catch let error {
				print(error.localizedDescription)
			}
			*/
		})
		task.resume()

		UserDefaults.standard.set("true", forKey: "isUserLoggedIn")
		UserDefaults.standard.synchronize()
		self.dismiss(animated: true, completion: nil)
		
		self.profile = DataBaseHelper.shareInstance.getProfileData()
		if self.profile.count == 0 {
			_ = DataBaseHelper.shareInstance.newProfile(object: ["aceite":"N"])
			self.profile = DataBaseHelper.shareInstance.getProfileData()
		}
		DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
		
		}
		
		txtUsuario.text = ""
		txtSenha.text = ""
		
		if (self.profile[0].aceite == "S") {
			self.performSegue(withIdentifier: "homeSegue", sender: nil)
		} else {
			let avisoAppAlert = UIAlertController(title: MyVariables.traducaoAlerta, message: MyVariables.traducaoAceiteTermos, preferredStyle: .alert)
			let mensagemApp = UIAlertAction(title: MyVariables.traducaoNao, style: .destructive) {
				(alert) -> Void in
				UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
				DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
					exit(EXIT_SUCCESS)
				})
			}
			
			let laterAction = UIAlertAction(title: MyVariables.traducaoSim, style: .cancel) {
				(alert) -> Void in
				self.dismiss(animated: true, completion: nil)
				self.ShowTerms()
			}
			
			avisoAppAlert.addAction(mensagemApp)
			avisoAppAlert.addAction(laterAction)
			present(avisoAppAlert, animated: true, completion: nil)
		}
	}
}

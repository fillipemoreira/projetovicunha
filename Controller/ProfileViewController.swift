//
//  ProfileViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 26/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

	var sNome :String = ""
	var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
	var profile = [Profile]()
	
	@IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblAutentica: UILabel!
    @IBOutlet weak var btLogOut: UIButton!
    
	var biometria: String!
	
	@IBAction func btLogOut(_ sender: UIButton) {
		self.showMessageResetApp()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func viewDidLoad() {
		
        super.viewDidLoad()
		
		self.aplicaValoresBotao(Sender: btLogOut)
		
        profile = DataBaseHelper.shareInstance.getProfileData()
        lblAutentica.text = profile[0].biometria
		// lblNome.text = profile[0].nome
		
		let dict = [MyVariables.traducaoBiometria: "S", MyVariables.traducaoAceite: "S"]
		_ = DataBaseHelper.shareInstance.updateProfile(object: dict , index: 0)
		profile = DataBaseHelper.shareInstance.getProfileData()
	}
	
	func showMessageResetApp(){

		let exitAppAlert = UIAlertController(title: MyVariables.traducaoTituloLogOut,
											 message: MyVariables.traducaoMensagemLogOut,
											 preferredStyle: .alert)
		
		let laterAction = UIAlertAction(title: MyVariables.traducaoBtLogOut, style: .destructive) {
			(alert) -> Void in
			UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
			DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
				exit(EXIT_SUCCESS)
			})
		}
		
		let resetApp = UIAlertAction(title: MyVariables.traducaoBtClose, style: .cancel) {
			(alert) -> Void in
			self.dismiss(animated: true, completion: nil)
		}
		
		exitAppAlert.addAction(resetApp)
		exitAppAlert.addAction(laterAction)
		present(exitAppAlert, animated: true, completion: nil)
	}
	
	func aplicaValoresBotao(Sender: UIButton) {
		Sender.layer.cornerRadius = Sender.frame.height / 2
	}
	
	func defineBotoes() {
		self.aplicaValoresBotao(Sender: btLogOut)
	}
}

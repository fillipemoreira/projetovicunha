//
//  DetailHVIViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 17/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//
import UIKit
import Foundation

class DetailHVIViewController:  UIViewController, UITableViewDelegate, UITableViewDataSource{
	
    private var detalhesHVI = [DetalhesHVI]()
    var lotes:[String] = []
	var IdContratoEmbarque = 0
	var motivo = ""
	private var documentos = [DocumentosHVI]()
	var idDocumento = 0
	//var idEmenda = 0
	var sExtensao = ""
	
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblObservacao: UILabel!
    @IBOutlet weak var lblQtdeEmbarque: UILabel!
    @IBOutlet weak var lblMercadoria: UILabel!
    @IBOutlet weak var lblDataGeracao: UILabel!
    @IBOutlet weak var lblEmbarque: UILabel!
    @IBOutlet weak var lblCorretora: UILabel!
    @IBOutlet weak var lblVendedor: UILabel!
    @IBOutlet weak var lblContrato: UILabel!
    //@IBOutlet weak var btAprovarE: UIButton!
	//@IBOutlet weak var btReprovarE: UIButton!
	
	//@IBOutlet weak var lblDocumentosEmenda: UILabel!
	
	//@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableViewDocumento: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
	//@IBOutlet weak var tableViewDocsEmenda: UITableView!
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView != self.tableViewDocumento) {
            if self.detalhesHVI.count > 0 {
                return self.detalhesHVI[0].Lotes.count
            }
            return 0
        } else {
            if self.documentos.count > 0 {
                return self.documentos.count
            }
            return 0
        }
	
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == self.tableViewDocumento) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "docHVICell", for: indexPath) as? DocsHVICell
			else { return UITableViewCell() }
           cell.lblNome.numberOfLines = 2
            cell.lblNome.text = "\(self.documentos[indexPath.row].Nome)"
            
            cell.lblDocumento.text = "\(self.documentos[indexPath.row].Documento)"
            
            cell.lblTipo.text = "\(self.documentos[indexPath.row].Tipo)"
            
            cell.lblData.text = "\(self.documentos[indexPath.row].Data)"
		
		
            self.activityIndicator.stopAnimating()
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellLotes", for: indexPath) as? LoteCell
                else { return UITableViewCell() }
            
            cell.lblNome.text = "\(self.lotes[indexPath.row])"
            
            
            self.activityIndicator.stopAnimating()
            return cell
        }
	}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row % 2 == 0) {
            cell.backgroundColor = UIColor.lightGray
        } else {
            cell.backgroundColor = UIColor.white
        }
    }
	
	
	
	private func carregaDadosDetalhesHVI() {
		let apiHVI = APIHVI()
		apiHVI.iID = self.IdContratoEmbarque
		apiHVI.executeGetDetalhesHVI() {arrayOfHVIPendentesLocal in
			self.detalhesHVI = arrayOfHVIPendentesLocal
			if (self.detalhesHVI.count > 0) {
				DispatchQueue.main.async {
					self.lblQtdeEmbarque.text = self.detalhesHVI[0].QtdeEmbarque.description
					self.lblContrato.text = self.detalhesHVI[0].Contrato
					self.lblVendedor.text = self.detalhesHVI[0].Vendedor
					self.lblCorretora.text = self.detalhesHVI[0].Corretora
					self.lblEmbarque.text = self.detalhesHVI[0].Embarque.description
					self.lblMercadoria.text = self.detalhesHVI[0].Mercadoria
					self.lblDataGeracao.text = self.detalhesHVI[0].DataGeracao
					self.lblObservacao.text = self.detalhesHVI[0].Observacao
                    self.lotes = self.detalhesHVI[0].Lotes
				}
				DispatchQueue.main.async {
					self.tableView.reloadData()
				}
			}
			self.activityIndicator.stopAnimating()
			
		}
	}
	
	private func carregaDadosDocumentos() {
		let apiHVI = APIHVI()
		apiHVI.iID = self.IdContratoEmbarque
		apiHVI.executeGetDocumentosByContrato() {arrayOfDocumentosLocal in
			self.documentos = arrayOfDocumentosLocal
			DispatchQueue.main.async {
				self.tableViewDocumento.reloadData()
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let img = UIImage()
		self.navigationController?.navigationBar.shadowImage = img
		self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
		activityIndicator.startAnimating()
		self.carregaDadosDetalhesHVI()
		self.carregaDadosDocumentos()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.title = "Detalhes HVI"
	}
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		self.tableViewDocumento.delegate = self
		self.tableViewDocumento.dataSource = self
		
		self.tableViewDocumento.allowsSelection = true
		//self.tableViewDocsEmenda.delaysContentTouches = false
		
		//self.title = "Documentos Emenda"
		
		activityIndicator.startAnimating()
		
		//self.defineBotoes()
		//self.defineGrades()
	}
}
	/*
	func defineGrades() {
		tableViewDocsEmenda.layer.borderWidth = MyConstants.borderTableView
		tableViewDocsEmenda.layer.borderColor = MyConstants.colorTableView
	}
	
	func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
		sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.borderWidth = 2
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	func defineBotoes() {
		self.aplicaValoresBotao(sender: btAprovarE)
		self.aplicaValoresBotao(sender: btReprovarE)
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.idDocumento = self.documentosEmendaF[indexPath.row].Documento
		
		let sArquivo = self.documentosEmendaF[indexPath.row].Nome
		let sSeparador = sArquivo.index(of: ".")!
		self.sExtensao = String(sArquivo[sSeparador...]).uppercased()
		
		tableView.deselectRow(at: indexPath, animated: false)
		
		self.goVisualizaDocEmenda()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let visualizaDocEViewController: VisualizaDocEViewController = segue.destination as! VisualizaDocEViewController
		visualizaDocEViewController.iDocumento = self.idDocumento
		visualizaDocEViewController.sExtensao = self.sExtensao
	}
	
	@IBAction func btAprovarE(_ sender: UIButton) {
		let alertController = UIAlertController(title: MyVariables.traducaoAprovacaoEmenda, message: MyVariables.traducaoConfirmaAprovacao, preferredStyle: .alert)
		
		let sim = UIAlertAction(title: MyVariables.traducaoSim, style: .default, handler: self.AprovarEmenda)
		alertController.addAction(sim)
		
		let nao = UIAlertAction(title: MyVariables.traducaoNao, style: .cancel)
		alertController.addAction(nao)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	@objc func goVisualizaDocEmenda() {
		self.performSegue(withIdentifier: "segueVisiualizaDocE", sender: nil)
	}
	
	@IBAction func btReprovarEmenda(_ sender: UIButton) {
		let alertController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: MyVariables.traducaoInformeMotivo, preferredStyle: .alert)
		
		let sim = UIAlertAction(title: MyVariables.traducaoReprovacao, style: .default, handler: {action in
			self.motivo = alertController.textFields![0].text!
			
			if (self.motivo != "") {
				let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: MyVariables.traducaoReprovadoSucesso  + ": " + self.motivo, preferredStyle: .alert)
				let ok = UIAlertAction(title: "OK", style: .default, handler: self.HistoryBackEmenda)
				confirmacaoController.addAction(ok)
				self.ReprovarEmenda()
				self.present(confirmacaoController, animated: true, completion: nil)
			} else {
				AppUtility.showAlertController(sender: self, "Erro", "campoMotivoObrigatorio")
			}
		})
		alertController.addAction(sim)
		
		alertController.addTextField { (textField) in
			textField.placeholder = MyVariables.traducaoMotivo
		}
		
		let nao = UIAlertAction(title: MyVariables.traducaoNao, style: .cancel)
		alertController.addAction(nao)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	func AprovarEmenda(alert: UIAlertAction) {
		
		let apiEmenda = APIEmenda()
		let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(true), "dsMotivo": String("\"\"")] as [String : Any]
		
		self.activityIndicator.startAnimating()
		apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
			retornoLocal in
			
			let confirmacaoController = UIAlertController(title: MyVariables.traducaoAprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
			
			let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		self.activityIndicator.stopAnimating()
	}
	
	func ReprovarEmenda() {
		
		let apiEmenda = APIEmenda()
		let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(false), "dsMotivo": self.motivo] as [String : Any]
		
		self.activityIndicator.startAnimating()
		apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
			retornoLocal in
			
			let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
			
			let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
			confirmacaoController.addAction(ok)
			
			self.present(confirmacaoController, animated: true, completion: nil)
		}
		self.activityIndicator.stopAnimating()
	}
	
	
	func HistoryBackEmenda(alert: UIAlertAction) {
		self.navigationController?.popViewController(animated: true)
	}
	
	//	DocsEmendaViewController.idDocumento = self.iIdDocumento
	//	DocsEmendaViewController.idEmenda = self.iIdEmenda
 */


//
//  ContratosViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 01/11/18.
//  Copyright © 2018 Ronney Nigro. All rights reserved.
//

import UIKit

protocol SecondViewDelegate: class {
    func sendToThirdViewController()
}

class ContratosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
    weak var delegate: SecondViewDelegate? = nil
	private var arrayOfContratosPendentes = [ContratosPendentes]()
	var iIdAprovacaoContrato = 0
	var iIdContrato = 0

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var activityScreen: UIActivityIndicatorView!
    
    private func carregaDadosContratosPendentes() {
		
		DispatchQueue.main.async {
			self.activityScreen.startAnimating()
		}
		
		let apiContrato = APIContrato()
		apiContrato.executeGetContratosPendentes() {arrayOfContratosPendentesLocal in
			self.arrayOfContratosPendentes = arrayOfContratosPendentesLocal
			DispatchQueue.main.async {
                self.tableView.reloadData()
				self.activityScreen.stopAnimating()
            }
		}
		if (self.arrayOfContratosPendentes.count == 0) {
            self.activityScreen.stopAnimating()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let img = UIImage()
		self.navigationController?.navigationBar.shadowImage = img
    	self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
	
		self.carregaDadosContratosPendentes()
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.title = "Contratos Pendentes"
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		self.tableView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	
		self.setupNavBarButton()
        
		self.tableView.delegate = self
        self.tableView.dataSource = self
		
		self.tableView.allowsSelection = true
		self.tableView.delaysContentTouches = false
	}
	
	func setupNavBarButton() {
		let searchImage = UIImage(named: "iconSearch.png")?.withRenderingMode(.alwaysOriginal)
		_ = UIImage(named: "iconHamburger.png")?.withRenderingMode(.alwaysOriginal)
		
		let searchBarButtonItem = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: #selector(handleSearch))
		
		let moreButton = UIBarButtonItem(image: UIImage(named: "iconHamburger.png"), style: .plain, target: self, action: #selector(handleMore))
		
		navigationItem.rightBarButtonItems = [moreButton, searchBarButtonItem]
	}
	
	@objc private func handleSearch() {
		print(456)
	}
	
	lazy var settingsLauncher: SettingsLauncher = {
		let launcher = SettingsLauncher()
		return launcher
	}()
	
	@objc private func handleMore() {
		settingsLauncher.showSettings()
		// showControllerForSetting()
	}
	
	func showControllerForSetting(settingLocal: Settings) {
		let dummySettingsViewController = UIViewController()
		dummySettingsViewController.view.backgroundColor = UIColor.white
		dummySettingsViewController.navigationItem.title = settingLocal.name.rawValue
		navigationController?.navigationBar.tintColor = UIColor.black
		navigationController?.pushViewController(dummySettingsViewController, animated: true)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.arrayOfContratosPendentes.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
			else { return UITableViewCell() }
		
		cell.lblContrato.text = self.arrayOfContratosPendentes[indexPath.row].Contrato
		cell.lblDataGeracao.text = self.arrayOfContratosPendentes[indexPath.row].DataGeracao
		cell.lblVendedor.text = self.arrayOfContratosPendentes[indexPath.row].Vendedor
		cell.lblTipoPreco.text = self.arrayOfContratosPendentes[indexPath.row].TipoPreco
		cell.lblIdAprovacao.text = "\(self.arrayOfContratosPendentes[indexPath.row].IdAprovacaoContrato)"
		
		return cell
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
		
		self.iIdAprovacaoContrato = self.arrayOfContratosPendentes[indexPath.row].IdAprovacaoContrato
		self.iIdContrato = self.arrayOfContratosPendentes[indexPath.row].IdContrato
		
		tableView.deselectRow(at: indexPath, animated: false)

		goDetails()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let DetailContratoViewController: DetailContratoViewController = segue.destination as! DetailContratoViewController
		DetailContratoViewController.iIdAprovacaoContrato = self.iIdAprovacaoContrato
		DetailContratoViewController.idContrato = self.iIdContrato
	}
	
	@objc func goDetails() {
		self.performSegue(withIdentifier: "segueDetalhesContrato", sender: nil)
	}
}

//
//  IndiceReferenciaViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 25/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class IndiceReferenciaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
    var detalhesContrato = [DetalhesContrato]()
	var activityIndicatorView: UIActivityIndicatorView!
    var iID = 0
	
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if (self.detalhesContrato[0].IndicesReferencia != nil) {
			return self.detalhesContrato[0].IndicesReferencia.descricoes.count
		} else {
			return 0
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContratosCell", for: indexPath) as? ContratosCell
			else { return UITableViewCell() }
		
        cell.lblIndiceReferencia.text = self.detalhesContrato[0].IndicesReferencia.descricoes[indexPath.row]
		
		self.activityIndicatorView.stopAnimating()
		return cell
	}
	
	private func carregaDadosIndiceReferenciaContrato() {
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}
	
	override func loadView() {
		super.loadView()
		activityIndicatorView = UIActivityIndicatorView(style: .gray)
		
		tableView.backgroundView = activityIndicatorView
	}
	
	override func viewDidAppear(_ animated: Bool) {
		carregaDadosIndiceReferenciaContrato()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		activityIndicatorView.startAnimating()
	}
	
	override func viewDidLoad() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
		
		self.tableView.allowsSelection = true
		self.tableView.delaysContentTouches = false
		
		self.title = "Índice Referência"
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
}

//
//  DetailEmendaFViewController.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 29/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DetailEmendaFViewController:  UIViewController, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
	private var detalhesEmendaF = [DetalhesEmenda]()
	var idAprovacaoEmenda = 0
	var motivo = ""
	private var documentosEmendaF = [DocumentosEmenda]()
	var idDocumento = 0
	var idEmenda = 0
	var sExtensao = ""


	
    @IBOutlet weak var lblTEmendaF: UILabel!
    @IBOutlet weak var lblTContratoF: UILabel!
    @IBOutlet weak var lblTVendedorF: UILabel!
    @IBOutlet weak var lblTEmissaoF: UILabel!
    @IBOutlet weak var lblTTipoF: UILabel!
    @IBOutlet weak var lblTCorretoraF: UILabel!
    @IBOutlet weak var lblTFixadoF: UILabel!
    @IBOutlet weak var lblTQtdeContratoF: UILabel!
    @IBOutlet weak var lblTQtdeFixadaF: UILabel!
    @IBOutlet weak var lblTSaldoF: UILabel!
    
    @IBOutlet weak var lblEmendaF: UILabel!
    @IBOutlet weak var lblContratoF: UILabel!
    @IBOutlet weak var lblVendedorF: UILabel!
    @IBOutlet weak var lblEmissaoF: UILabel!
    @IBOutlet weak var lblTipoF: UILabel!
    @IBOutlet weak var lblFixadoF: UILabel!
    @IBOutlet weak var lblCorretoraF: UILabel!
    @IBOutlet weak var lblQtdeContratoF: UILabel!
    @IBOutlet weak var lblQtdeFixadaF: UILabel!
    @IBOutlet weak var lblSaldoF: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var lblPrecosF: UILabel!
    @IBOutlet weak var tableViewPrecos: UITableView!
    
    @IBOutlet weak var lblTObservacoesF: UILabel!
    @IBOutlet weak var lblObservacoesF: UILabel!
   
    @IBOutlet weak var lblTFixacoesF: UILabel!
  
    @IBOutlet weak var lblTDataFixacao: UILabel!
    @IBOutlet weak var lblTPrecoNY: UILabel!
    @IBOutlet weak var lblTBasis: UILabel!
    @IBOutlet weak var lblTPrecoTotalF: UILabel!
    
    @IBOutlet weak var lblMediaTotal: UILabel!
    @IBOutlet weak var lblTMediaAnterioresF: UILabel!
    
    @IBOutlet weak var lblDataFixacaoF: UILabel!
    @IBOutlet weak var lblPrecoNY: UILabel!
    @IBOutlet weak var lblBasis: UILabel!
    @IBOutlet weak var lblPrecoTotalF: UILabel!
    
    @IBOutlet weak var lblMediaTotalF: UILabel!
    @IBOutlet weak var lblMediaAnterioresF: UILabel!
    
    @IBOutlet weak var lblDocumentosF: UILabel!
    @IBOutlet weak var tableViewDocumentos: UITableView!
    
    @IBOutlet weak var btAprovarF: UIButton!
    @IBOutlet weak var btReprovarF: UIButton!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		var count = 0
		
		if (tableView == tableViewPrecos) {
			count = self.detalhesEmendaF.count
			if (count > 0) {
				count = self.detalhesEmendaF[0].Preco.count
			}
		} else if (tableView == tableViewDocumentos)  {
			count = self.documentosEmendaF.count
			if (count > 0) {
				count = self.documentosEmendaF.count
			}
		}
		
		return count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cellMaster : UITableViewCell?
		
		// Preços
		if (tableView == self.tableViewPrecos) {
			
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "PrecosEmendaCell", for: indexPath) as? PrecosEmendaCell
				else { return UITableViewCell() }
			
			cell.lblEntregaF.text = self.detalhesEmendaF[0].Preco[indexPath.row].dsNome
			
			cellMaster = cell
		}
		
		if (tableView == self.tableViewDocumentos) {
		
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "DocsEmendaCell", for: indexPath) as? DocsEmendaCell
				else { return UITableViewCell() }
		
			cell.lblDocumentoF.text = "\(self.documentosEmendaF[indexPath.row].Documento)"
			cell.lblNomeDocF.text = self.documentosEmendaF[indexPath.row].Nome
			cell.lblTipoDocF.text = self.documentosEmendaF[indexPath.row].Tipo
			cell.lblDataDocF.text = self.documentosEmendaF[indexPath.row].Data

			self.activityIndicator.stopAnimating()
			
			cellMaster = cell
		}
		
		return cellMaster!
	}
	
	private func exibeTextoDetalhesEntregaF() {
	
		self.lblTEmendaF.isHidden = false
		self.lblTContratoF.isHidden = false
		self.lblTVendedorF.isHidden = false
		self.lblTEmissaoF.isHidden = false
		self.lblTTipoF.isHidden = false
		self.lblTCorretoraF.isHidden = false
		self.lblTFixadoF.isHidden = false
		self.lblTQtdeContratoF.isHidden = false
		self.lblTQtdeFixadaF.isHidden = false
		self.lblTSaldoF.isHidden = false
		
		self.lblPrecoTotalF.isHidden = false
        
        self.lblPrecosF.isHidden = false
		self.tableViewPrecos.isHidden = false
		
		self.lblEmendaF.isHidden = false
		self.lblContratoF.isHidden = false
		self.lblVendedorF.isHidden = false
		self.lblEmissaoF.isHidden = false
		self.lblTipoF.isHidden = false
		self.lblCorretoraF.isHidden = false
		self.lblFixadoF.isHidden = false
		self.lblQtdeContratoF.isHidden = false
		self.lblQtdeFixadaF.isHidden = false
		self.lblSaldoF.isHidden = false
		
		self.lblTObservacoesF.isHidden = false
		self.lblObservacoesF.isHidden = false
		
		self.lblTFixacoesF.isHidden = false
		
		self.lblTDataFixacao.isHidden = false
        self.lblTBasis.isHidden = false
		self.lblTPrecoNY.isHidden = false
		self.lblBasis.isHidden = false
		self.lblTPrecoTotalF.isHidden = false
		
		self.lblDataFixacaoF.isHidden = false
		self.lblPrecoNY.isHidden = false
		self.lblBasis.isHidden = false
		self.lblPrecoTotalF.isHidden = false
		
        self.tableViewPrecos.isHidden = false
		
        self.lblMediaTotal.isHidden = false
        self.lblMediaTotalF.isHidden = false
        
        self.lblTMediaAnterioresF.isHidden = false
        self.lblMediaAnterioresF.isHidden = false
        
        self.lblDocumentosF.isHidden = false
        self.tableViewDocumentos.isHidden = false
        
		self.btAprovarF.isHidden = false
		self.btReprovarF.isHidden = false
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	private func carregaDadosDocumentosEmendaF() {
		let apiEmendaDoc = APIEmenda()
		apiEmendaDoc.iID = self.idEmenda
		apiEmendaDoc.executeGetDocumentosByEmenda() {documentosEmendaFLocal in
			self.documentosEmendaF = documentosEmendaFLocal
			DispatchQueue.main.async {
				self.tableViewDocumentos.reloadData()
			}
		}
	}
	
	private func carregaDetalhesEmendaF() {
		let apiEmenda = APIEmenda()
		apiEmenda.iID = self.idAprovacaoEmenda
		apiEmenda.executeGetDetalhesEmenda() { EmendasFPendentesFLocal in
			self.detalhesEmendaF = EmendasFPendentesFLocal
			if (self.detalhesEmendaF.count > 0) {
				DispatchQueue.main.async {
					self.lblEmendaF.text = self.detalhesEmendaF[0].Emenda
					self.lblContratoF.text = self.detalhesEmendaF[0].Contrato
					self.lblVendedorF.text = self.detalhesEmendaF[0].Vendedor
					self.lblEmissaoF.text = self.detalhesEmendaF[0].Emissao
					self.lblTipoF.text = self.detalhesEmendaF[0].Tipo
					self.lblCorretoraF.text = self.detalhesEmendaF[0].Corretora
					self.lblFixadoF.text = self.detalhesEmendaF[0].FixadoPor
					self.lblQtdeContratoF.text = String(self.detalhesEmendaF[0].QtdeContrato)
					self.lblQtdeFixadaF.text = String(self.detalhesEmendaF[0].QtdeFixada)
					self.lblSaldoF.text = String(self.detalhesEmendaF[0].SaldoAFixar)
					
					self.lblObservacoesF.text = self.detalhesEmendaF[0].ObsEmenda
					
					self.lblDataFixacaoF.text = self.detalhesEmendaF[0].Fixacoes.GridFixacoes[0].DataFixacao
					self.lblPrecoNY.text = String(self.detalhesEmendaF[0].Fixacoes.GridFixacoes[0].PrecoNY)
					self.lblBasis.text = String(self.detalhesEmendaF[0].Fixacoes.GridFixacoes[0].Basis)
					self.lblPrecoTotalF.text = String(self.detalhesEmendaF[0].Fixacoes.GridFixacoes[0].PrecoTotal)
					self.lblMediaTotal.text = String(self.detalhesEmendaF[0].Fixacoes.MediaTotal)
					self.lblMediaAnterioresF.text = String(self.detalhesEmendaF[0].Fixacoes.MediaAnteriores)
					
					self.tableViewPrecos.reloadData()
				}
			}
			self.activityIndicator.stopAnimating()
			self.exibeTextoDetalhesEntregaF()
		}
	}
	
	override func loadView() {
		super.loadView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.title = "Detalhes Emenda Fixada"
	}
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		self.tableViewPrecos.delegate = self
		self.tableViewPrecos.dataSource = self
		
		self.tableViewDocumentos.allowsSelection = true
		self.tableViewDocumentos.delaysContentTouches = false
		
		activityIndicator.startAnimating()
		
		self.defineBotoes()
		self.defineGrades()
	}
	
	func defineGrades() {
		tableViewPrecos.layer.borderWidth = MyConstants.borderTableView
		tableViewPrecos.layer.borderColor = MyConstants.colorTableView
		
		tableViewDocumentos.layer.borderWidth = MyConstants.borderTableView
		tableViewDocumentos.layer.borderColor = MyConstants.colorTableView
	}
	
	
	private func defineBotoes() {
		self.aplicaValoresBotao(sender: btAprovarF)
		self.aplicaValoresBotao(sender: btReprovarF)
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if (tableView == tableViewDocumentos) {
			self.idDocumento = self.documentosEmendaF[indexPath.row].Documento
		
			let sArquivo = self.documentosEmendaF[indexPath.row].Nome
			let sSeparador = sArquivo.index(of: ".")!
			self.sExtensao = String(sArquivo[sSeparador...]).uppercased()
		
			tableView.deselectRow(at: indexPath, animated: false)
		
			self.goVisualizaDocEmendaF()
		} else {
			let array = self.detalhesEmendaF[0].Preco[indexPath.row].Entregas
			let sString = (array.joined(separator: "\n"))
			let newString = sString.replacingOccurrences(of: "Entrega:", with: "")

			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, self.detalhesEmendaF[0].Preco[indexPath.row].dsNome, newString)
		}
	}
	
	@objc func goVisualizaDocEmendaF() {
		self.performSegue(withIdentifier: "segueVisiualizaDocEF", sender: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let visualizaDocEViewController: VisualizaDocEViewController = segue.destination as! VisualizaDocEViewController
		visualizaDocEViewController.iDocumento = self.idDocumento
		visualizaDocEViewController.sExtensao = self.sExtensao
	}
	
	func aplicaValoresBotao(sender: UIButton) {
		sender.applyGradient(colours: [UIColor.magenta, UIColor.lightGray,  UIColor.lightGray], locations: [0.0, 0.5, 1.0])
		
		sender.layer.cornerRadius = sender.frame.height / 2
		sender.layer.borderColor = UIColor.white.cgColor
		sender.layer.borderWidth = 2
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightGray
		} else {
			cell.backgroundColor = UIColor.white
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		self.carregaDetalhesEmendaF()
		self.carregaDadosDocumentosEmendaF()
	}
    
    @IBAction func aprovarEmenda(_ sender: Any) {
        let apiEmenda = APIEmenda()
        let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(true), "dsMotivo": String("\"\"")] as [String : Any]
        
        self.activityIndicator.startAnimating()
		apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
            retornoLocal in
            
            let confirmacaoController = UIAlertController(title: MyVariables.traducaoAprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
            confirmacaoController.addAction(ok)
            
            self.present(confirmacaoController, animated: true, completion: nil)
        }
        self.activityIndicator.stopAnimating()
    }
    
	/*func AprovarEmenda(alert: UIAlertAction) {
		
		
	}*/
    @IBAction func reprovarEmenda(_ sender: Any) {
    
        let apiEmenda = APIEmenda()
        let parametersLocal = ["idAprovacao": self.idAprovacaoEmenda, "aprova": String(false), "dsMotivo": self.motivo] as [String : Any]
        
        self.activityIndicator.startAnimating()
        apiEmenda.executePutAprovarReprovarEmenda(parametersLocal:parametersLocal) {
            retornoLocal in
            
            let confirmacaoController = UIAlertController(title: MyVariables.traducaoReprovacaoEmenda, message: retornoLocal, preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .default, handler: self.HistoryBackEmenda)
            confirmacaoController.addAction(ok)
            
            self.present(confirmacaoController, animated: true, completion: nil)
        }
        self.activityIndicator.stopAnimating()
    }
    /*func ReprovarEmenda() {
		
		
	}*/
	
	
	func HistoryBackEmenda(alert: UIAlertAction) {
		self.navigationController?.popViewController(animated: true)
	}
}

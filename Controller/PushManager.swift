//
//  PushManager.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 23/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import Foundation
import UserNotifications
import PusherSwift

class PushManager  {

	static var shared = PushManager()
	let center = UNUserNotificationCenter.current()
	
	func requestAutorization() {
		center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
			if error != nil {
				print("Erro: \(String(describing: error))")
			}
		}
	}
	
	func sendLocalPush(_ message: String) {
		let content = UNMutableNotificationContent()
		content.title = NSString.localizedUserNotificationString(forKey: "Important Notification", arguments: nil)
		content.body = NSString.localizedUserNotificationString(forKey: message, arguments: nil)
		
		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
		let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
		
		UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
		
		center.add(request) {(error) in
			if error != nil {
				print("Erro: \(String(describing: error))")
			}
		}
	}
}

//
//  DetalhesHVI.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 17/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

class DetalhesHVI: Codable {
	
	
	let Contrato: String
	let Vendedor: String
	let Corretora: String
	let Embarque: Int
	let DataGeracao: String
	let Mercadoria: String
	let QtdeEmbarque: Double
	let Observacao: String
	let Lotes: [String]
	

	
	class func detalhesHVIJSON(paramEntrada: String) -> [DetalhesHVI] {
		var arrayOfDetalhesHVI: [DetalhesHVI] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDetalhesHVI = try JSONDecoder().decode([DetalhesHVI].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DetalhesHVI - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDetalhesHVI
	}
}

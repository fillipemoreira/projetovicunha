//
//  DetalhesContrato.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 13/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DetalhesContrato: Codable {

	let Contrato: String
	let Emissao: String
	let Vendedor: String
	let Corretora: String
	let Fornecedor: String
	let Regiao: String
	let Entrega: String
	let Safra: String!
	let Quantidade: String
	let Qualidade: Qualidade
	let Preco: [Preco]
	let IndicesReferencia: IndicesReferencia!
	let Pagamento: String!
	let ObsContrato: String!

	class Qualidade: Codable {
		
		let qualidade: String
		let obsQualidade: String
	}
	
	class Preco: Codable {
	
		let idIndicePreco: Int!
		let dsNome: String
		let Entregas: [String]
		let vlPreco: Double!
	}
	
	class IndicesReferencia: Codable {
		
		let data: String
		let descricoes: [String]
		
	}

	
	
	class func detalhesContratoJSON(paramEntrada: String) -> [DetalhesContrato] {
		var arrayOfDetalhesContrato: [DetalhesContrato] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDetalhesContrato = try JSONDecoder().decode([DetalhesContrato].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DetalhesContrato - Error serializing json",  "\(jsonErr)")
		}
		return arrayOfDetalhesContrato
	}
}

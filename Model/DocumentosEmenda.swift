//
//  DocumentosEmenda.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 08/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DocumentosEmenda: Codable {
	
	let Documento: Int
	let Nome: String
	let Tipo: String
	let Data: String
	
	class func documentosEmendaJSON(paramEntrada: String) -> [DocumentosEmenda] {
		var arrayOfDocumentosEmenda: [DocumentosEmenda] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDocumentosEmenda = try JSONDecoder().decode([DocumentosEmenda].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DocumentosEmenda - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDocumentosEmenda
	}
}

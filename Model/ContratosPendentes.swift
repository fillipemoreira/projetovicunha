//
//  ContratosPendentes.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 14/11/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import UIKit

class ContratosPendentes: Codable {
	
	let Contrato: String
	let IdContrato: Int
	let DataGeracao: String
	let Vendedor: String
	let TipoPreco: String
	let IdAprovacaoContrato: Int
	
	class func contratosPendentesJSON(paramEntrada: String) -> [ContratosPendentes] {
		var arrayOfContratosPendentes: [ContratosPendentes] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfContratosPendentes = try JSONDecoder().decode([ContratosPendentes].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "ContratosPendentes - Error serializing json",  "\(jsonErr)")
		}
		return arrayOfContratosPendentes
	}
}

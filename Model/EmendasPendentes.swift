//
//  EmendasPendentes.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 30/01/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class EmendasPendentes: Codable {
	
	let Emenda: String
	let IdEmenda: Int
	let Contrato: String
	let DataGeracao: String
	let Vendedor: String
	let Tipo: String
	let IdAprovacaoEmenda: Int
	
	class func emendasPendentesJSON(paramEntrada: String) -> [EmendasPendentes] {
		var arrayOfEmendasPendentes: [EmendasPendentes] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfEmendasPendentes = try JSONDecoder().decode([EmendasPendentes].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "EmendasPendentes - Error serializing json",  "\(jsonErr)")
		}
		return arrayOfEmendasPendentes
	}
}

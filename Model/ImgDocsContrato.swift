//
//  ImgDocsContrato.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 28/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class ImgDocsContrato: Codable {
	
	let imgDocumento: String
	
	class func imgDocsContratoJSON(paramEntrada: String) -> [ImgDocsContrato] {
		var arrayOfimgDocsContrato: [ImgDocsContrato] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfimgDocsContrato = try JSONDecoder().decode([ImgDocsContrato].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "ImgDocsContrato - Error serializing json", "\(jsonErr)")
		}
		return arrayOfimgDocsContrato
	}
}

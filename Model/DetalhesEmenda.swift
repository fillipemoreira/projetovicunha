//
//  DetalhesEmenda.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 07/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

class DetalhesEmenda: Codable {
	
	let Emenda: String
	let IdEmenda: Int!
	let Contrato: String
	let Vendedor: String
	let Emissao: String
	let Tipo: String
	let Corretora: String
	let FixadoPor: String!
	let QtdeContrato: Int!
	let QtdeFixada: Int!
	let SaldoAFixar: Int!
	let Preco: [Preco]!
	let Descricao: String!
	let ObsEmenda: String!
	let Fixacoes: Fixacoes!
	
	class Preco: Codable {
		
		let idIndicePreco: Int!
		let dsNome: String
		let Entregas: [String]
		let vlPreco: Double!
	}
	
	class Fixacoes: Codable {
		
		let GridFixacoes: [GridFixacoes]
		let MediaTotal: Double
		let MediaAnteriores: Double
	}
	
	class GridFixacoes: Codable {
		
		let Emenda: String
		let DataFixacao: String
		let QtdeFixada: Int
		let PrecoNY: Double
		let Basis: Double
		let PrecoTotal: Double
	}
	
	class func detalhesEmendaJSON(paramEntrada: String) -> [DetalhesEmenda] {
		var arrayOfDetalhesEmenda: [DetalhesEmenda] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDetalhesEmenda = try JSONDecoder().decode([DetalhesEmenda].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DetalhesEmenda - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDetalhesEmenda
	}
}

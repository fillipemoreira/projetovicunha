//
//  DocumentosHVI.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 20/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DocumentosHVI: Codable {

	let Documento: Int
	let Nome: String
	let Tipo: String
	let Data: String
	
	class func documentosHVIJSON(paramEntrada: String) -> [DocumentosHVI] {
		var arrayOfDocumentosHVI: [DocumentosHVI] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDocumentosHVI = try JSONDecoder().decode([DocumentosHVI].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DocumentosHVI - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDocumentosHVI
	}

}

//
//  HVIPendentes.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 14/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

class HVIPendentes: Codable {
	
	let Contrato: String
	let DataGeracao: String
	let Embarque: Int
	let IdContratoEmbarque: Int
	let Vendedor: String
	let QtdeEmbarque: Double
	
	
	class func hviJSON(paramEntrada: String) -> [HVIPendentes] {
		var arrayOfHviPendentes: [HVIPendentes] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfHviPendentes = try JSONDecoder().decode([HVIPendentes].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "HVIPendentes - Error serializing json",  "\(jsonErr)")
		}
		return arrayOfHviPendentes
	}
}

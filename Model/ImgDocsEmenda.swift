//
//  imgDocsEmenda.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 11/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

class ImgDocsEmenda: Codable {
	
	let imgDocumento: String
	
	class func imgDocsEmendaJSON(paramEntrada: String) -> [ImgDocsEmenda] {
		var arrayOfimgDocsEmenda: [ImgDocsEmenda] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfimgDocsEmenda = try JSONDecoder().decode([ImgDocsEmenda].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "ImgDocsEmenda - Error serializing json", "\(jsonErr)")
		}
		return arrayOfimgDocsEmenda
	}
	
}

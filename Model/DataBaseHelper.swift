//
//  DataBaseHelper.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 10/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataBaseHelper{
	
	static var shareInstance = DataBaseHelper()
	let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
	
	func deleteProfile(index: Int) -> [Profile] {
		var profile = getProfileData()
		context?.delete(profile[index])
		profile.remove(at: index)

		do {
			try context!.save()
		} catch {
			let saveError = error as NSError
			print("Error on Delete Profile: \(saveError)")
		}
		return profile
	}
	
	func updateProfile(object:[String: String], index: Int) -> Error? {
		let profile = getProfileData()
		profile[index].biometria = object["biometria"]
		profile[index].aceite = object["aceite"]

		do {
			try context!.save()
			return nil
		} catch {
			print(error)
			return error
		}
	}
	
	func newProfile(object:[String: String]) -> Error? {
		let profile = NSEntityDescription.insertNewObject(forEntityName: "Profile", into: context!) as! Profile
		
		profile.aceite = object["aceite"]
		do {
			try context!.save()
		//	print(profile)
			return nil
		} catch {
			let saveError = error as NSError
			print("Error on Create Profile: \(saveError)")
			return error
		}
	}
	
	func getProfileData() -> [Profile] {
		var profile = [Profile]()
		let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Profile")
		
		do {
			profile = try context?.fetch(fetchRequest) as! [Profile]
		} catch {
			print("Erro de Leitura: Profile")
		}
		return profile
	}
}

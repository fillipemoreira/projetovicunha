//
//  DetalhesPagamento.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 19/03/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DetalhesPagamento: Codable {
	
	let Contrato: String
	
	class func detalhesPagamentoJSON(paramEntrada: String) -> [DetalhesPagamento] {
		var arrayOfDetalhesPagamento: [DetalhesPagamento] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDetalhesPagamento = try JSONDecoder().decode([DetalhesPagamento].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DetlahesPagamento - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDetalhesPagamento
	}
}

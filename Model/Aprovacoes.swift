//
//  Aprovacoes.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 11/01/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class Aprovacoes: Codable {
	
	let vAprovacoesContrato: Int
	let vAprovacoesEmenda: Int
	let vAprovacoesEmbarqueLote: Int
	let vAprovacoesPagamento: Int
	
	class func aprovacoesJSON(paramEntrada: String) -> [Aprovacoes] {
		var arrayOfAprovacoes: [Aprovacoes] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfAprovacoes = try JSONDecoder().decode([Aprovacoes].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "Aprovacoes - Error serializing json", "\(jsonErr)")
		}
		return arrayOfAprovacoes
	}
}

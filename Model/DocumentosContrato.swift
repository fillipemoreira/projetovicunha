//
//  DocumentosContrato.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 27/02/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import UIKit

class DocumentosContrato: Codable {

	let Documento: Int
	let Nome: String
	let Tipo: String
	let Data: String
	
	class func documentosContratoJSON(paramEntrada: String) -> [DocumentosContrato] {
		var arrayOfDocumentosContrato: [DocumentosContrato] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfDocumentosContrato = try JSONDecoder().decode([DocumentosContrato].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "DocumentosContrato - Error serializing json", "\(jsonErr)")
		}
		return arrayOfDocumentosContrato
	}
}

//
//  PagamentosPendentes.swift
//  Projeto_Vicunha
//
//  Created by Ronney Nigro on 17/05/19.
//  Copyright © 2019 Quality Software. All rights reserved.
//

import Foundation

class PagamentosPendentes: Codable {
	
	let IdAprovacaoPagamento: Int
	let Contrato: String
	let DataGeracao: String
	let Pagamento: Int
	let IdContratoPagamento: Int
	let Vendedor: String
	let TipoPagamento: String
	let ValorAPagar: Double
	
	
	
	class func pagamentosPendentesJSON(paramEntrada: String) -> [PagamentosPendentes] {
		var arrayOfPagamentosPendentes: [PagamentosPendentes] = []
		let entrada = paramEntrada.data(using: .utf8)!
		do {
			arrayOfPagamentosPendentes = try JSONDecoder().decode([PagamentosPendentes].self, from: entrada)
		} catch let jsonErr {
			let topController = AppUtility.topController()
			AppUtility.showAlertController(sender: topController, "PagamentosPendentes - Error serializing json",  "\(jsonErr)")
		}
		return arrayOfPagamentosPendentes
	}
}

//
//  Profile+CoreDataProperties.swift
//  ProjetoProjeto_VicunhaVicunha
//
//  Created by Ronney Nigro on 07/12/18.
//  Copyright © 2018 Quality Software. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var biometria: String?
	@NSManaged public var aceite: String?

}
